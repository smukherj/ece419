import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.Watcher.Event.EventType;

import java.io.*;
import java.util.concurrent.locks.*;
import java.net.*;
import java.util.*;

class JobTracker
{
	private static Watcher m_watcher;
	private static ReentrantLock m_lock;
	private static Condition m_cond;
	private static int m_num_partitions;
	private static ZkConnector m_zkc;
	private static ArrayList<Socket> m_socket_list;
	private static ArrayList<ObjectOutputStream> m_ostream_list;
	private static int m_partitions_in_progress;
	
	private static final String JOB_TRACKER_ZNODE = "/JobTracker";
	private static final int JOB_TRACKER_CHECK_INTERVAL = 1000;
	private static final int WORKER_LOAD_FACTOR = 4;
	
	public static void sleep()
	{
		try
		{
			Thread.sleep(JOB_TRACKER_CHECK_INTERVAL);
		}
		catch(InterruptedException e)
		{
			
		}
	}
	
	public static void get_num_partitions()
	{
		MyUtils.info("Trying to get number of partitions from file server");
		while(true)
		{
			try
			{
				String data = new String(m_zkc.getZooKeeper().getData(FileServer.FILE_SERVER_ZNODE, false, null));
				String hostname = MyUtils.parseHost(data);
				int port = MyUtils.parsePort(data);
				
				Socket socket = new Socket(hostname, port);
				ObjectOutputStream ostream = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream istream = new ObjectInputStream(socket.getInputStream());
				MessagePacket m = new MessagePacket();
				m.type = MessagePacket.MessageType.GET_NUM_PARTITIONS;
				ostream.writeObject(m);
				m = (MessagePacket)istream.readObject();
				if(m.type != MessagePacket.MessageType.PARTITION)
				{
					throw new Exception("FileServer replied with unexpected message of type " + m.type.name());
				}
				else if(m.num_elements <= 0)
				{
					throw new Exception("FileServer says it has " + Integer.toString(m.num_elements) + " partitions. Nothing to do here");
				}
				m_num_partitions = m.num_elements;
			}
			catch(Exception e)
			{
				MyUtils.error(e.getMessage());
				sleep();
				continue;
			}
			break;
		}
		MyUtils.info("Data has " + m_num_partitions + " partitions of size " + MyUtils.PARTITION_SIZE);
	}

    public static void launch_primary_jobtracker()
	{
    	m_socket_list = new ArrayList<Socket>();
    	m_ostream_list = new ArrayList<ObjectOutputStream>();
    	MyUtils.attach_file_logger(new FileLogger("jobtracker.log"));
    	MyUtils.info("Launching primary job tracker");
    	get_num_partitions();
    	boolean no_progress_reported = false;
    	while(true)
    	{
    		m_socket_list.clear();
    		m_ostream_list.clear();
    		m_partitions_in_progress = 0;
    		try
    		{
    			List<String> jobs = get_jobs_in_progress();
    			List<Integer> workers = get_online_workers();
    			if(jobs.size() == 0 || workers.size() == 0)
    			{
    				if(!no_progress_reported)
    				{
    					no_progress_reported = true;
    					if(jobs.size() == 0)
    					{
    						MyUtils.info("No jobs available");
    					}
    					else
    					{
    						MyUtils.info("No online workers");
    					}
    				}
    			}
    			else
    			{
    				no_progress_reported = false;
    				MyUtils.info("Found " + jobs.size() + " jobs in progress and " + workers.size() + " online workers");
    			}
    			launch_jobs(jobs, workers);
    			sleep();
    		}
    		catch(Exception e)
    		{
    			MyUtils.error(e.getMessage());
    		}
    	}
	}
    
    public static List<ConnectionInfo> get_worker_connection_info(List<Integer> w)
    {
    	List<ConnectionInfo> workers = new ArrayList<ConnectionInfo>();
    	for(Integer wid : w)
    	{
    		String wpath = MyUtils.WORKERS_ZNODE + "/" + wid.toString();
    		try 
    		{
				String data = new String(m_zkc.getZooKeeper().getData(wpath, false, null));
				String host = MyUtils.parseHost(data);
				int port = MyUtils.parsePort(data);
				ConnectionInfo c = new ConnectionInfo();
				c.host = host;
				c.port = port;
				workers.add(c);
			} catch (Exception e) {
			}
    	}
    	return workers;
    }
    
    public static boolean launch_job_partition(String job, int partition, ConnectionInfo worker)
    throws Exception
    {
    	String donepath = MyUtils.JOBS_ZNODE + "/" + job + "/" + partition + "D";
    	boolean done = m_zkc.exists(donepath, null) != null;
    	if(done)
    	{
    		String data = new String(m_zkc.getZooKeeper().getData(donepath, false, null));
    		JobStatus j = JobStatus.parseString(data);
    		// This parition is done and we found the cracked password here
    		if(j.type == JobStatus.StatusType.DONE && j.cracked_password != null)
    		{
    			MyUtils.debug("    Cracked password for job " + job + " in partition " 
    		+ partition + "! '" + j.cracked_password + "'");
    			String jobpath = MyUtils.JOBS_ZNODE + "/" + job;
        		j.password = new String(job);
        		m_zkc.getZooKeeper().setData(jobpath, j.toString().getBytes(), -1);
    		}
    		return true;
    	}
    	
    	++m_partitions_in_progress;
    	String inprogresspath = MyUtils.JOBS_ZNODE + "/" + job + "/" + partition + "P";
    	boolean inprogress = m_zkc.exists(inprogresspath, null) != null;
    	if(inprogress)
    	{
    		return false;
    	}
    	
    	Socket sock = new Socket(worker.host, worker.port);
    	ObjectOutputStream ostream = new ObjectOutputStream(sock.getOutputStream());
    	MessagePacket m = new MessagePacket();
    	m.type = MessagePacket.MessageType.SUBMIT_JOB;
    	m.job = job;
    	m.partition = partition;
    	ostream.writeObject(m);
    	ostream.flush();
    	m_socket_list.add(sock);
    	m_ostream_list.add(ostream);
    	MyUtils.vdebug("    Launched job " + job + ", partition " + partition);
    	
    	return false;
    }
    
    public static void launch_job(String jobid, CircularWorkerList workers) throws Exception
    {
    	String jobpath = MyUtils.JOBS_ZNODE + "/" + jobid;
		String jobdata = new String(m_zkc.getZooKeeper().getData(jobpath, false, null));
		JobStatus job = JobStatus.parseString(jobdata);
		if(job.type == JobStatus.StatusType.READY)
		{
			job.type = JobStatus.StatusType.INPROGRESS;
			m_zkc.getZooKeeper().setData(jobpath, job.toString().getBytes(), -1);
		}
    	int done_partitions = 0;
    	for(int i = 0; i < m_num_partitions; ++i)
    	{
    		try
    		{
    			if(launch_job_partition(jobid, i, workers.next()))
    			{
    				++done_partitions;
    				MyUtils.vdebug("    Partition " + i + " is done for job " + jobid);
    			}
    		}
    		catch(Exception e)
    		{
    			MyUtils.vdebug("    Failed to launch job " + jobid + ", partition " + i);
    		}
    	}
    	
    	if(done_partitions == m_num_partitions)
    	{
    		// All partitions are done for this job. Mark it as done.
    		jobdata = new String(m_zkc.getZooKeeper().getData(jobpath, false, null));
    		job = JobStatus.parseString(jobdata);
    		// If the partition is already marked as done, it means the password was
    		// cracked and we have nothing to do
    		if(job.type != JobStatus.StatusType.DONE)
    		{
    			job.type = JobStatus.StatusType.DONE;
    			job.cracked_password = null;
    			m_zkc.getZooKeeper().setData(jobpath, job.toString().getBytes(), -1);
    		}
    	}
    	MyUtils.info("    Job " + jobid + " is " + String.format("%.0f", (done_partitions * 100.0) / m_num_partitions) + "% done");
    }
    
    // This function goes through jobs currently marked 'READY' or 'INPROGRESS'.
    // READY jobs are simply launched. INPROGRESS jobs are checked. If they
    // have paritions which neither have a 'INPROGRESS' znode nor a 'DONE'
    // znode, these jobs are launched again.
    public static void launch_jobs(List<String> jobs, List<Integer> workers)
    {
    	if(jobs.size() == 0 || workers.size() == 0)
    	{
    		return;
    	}
    	
    	List<ConnectionInfo> worker_cinfo = get_worker_connection_info(workers);
    	if(worker_cinfo.size() == 0)
    	{
    		MyUtils.debug("Failed to get connection information for any worker");
    		return;
    	}
        // Shuffle the worker list so that when we have a lot of workers
        // all workers are roughly equally likely to get a partition from this
        // job to balance out the load.
        Collections.shuffle(workers);

        CircularWorkerList worker_clist = new CircularWorkerList(worker_cinfo);
    	
    	for(String job : jobs)
    	{
    		try 
    		{
				launch_job(job, worker_clist);
			} catch (Exception e) {
				MyUtils.error("Job " + job + " " + e.getMessage());
			}
    		
    		// On average each worker should only be processing WORKER_LOAD_FACTOR 
    		// partitions at any given point in time.
    		if(m_partitions_in_progress >= (worker_cinfo.size() * WORKER_LOAD_FACTOR))
    		{
    			break;
    		}
    	}
    }
    
    public static List<String> get_jobs_in_progress() throws Exception
    {
    	ArrayList<String> joblist = new ArrayList<String>();
    	List<String> jobs = m_zkc.getZooKeeper().getChildren(MyUtils.JOBS_ZNODE, false);
    	for(String j : jobs)
    	{
    		String jobpath = MyUtils.JOBS_ZNODE + "/" + j;
    		String jobdata = new String(m_zkc.getZooKeeper().getData(jobpath, false, null));
    		JobStatus job = JobStatus.parseString(jobdata);
    		if(job.type != JobStatus.StatusType.DONE)
    		{
    			MyUtils.vdebug("Job " + j + " is not yet done");
    			joblist.add(j);
    		}
    	}
    	return joblist;
    }
    
    public static List<Integer> get_online_workers() throws Exception
    {
    	ArrayList<Integer> workerlist = new ArrayList<Integer>();
    	List<String> workers = m_zkc.getZooKeeper().getChildren(MyUtils.WORKERS_ZNODE, false);
    	for(String w : workers)
    	{
    		workerlist.add(new Integer(w));
    	}
    	return workerlist;
    }
    
    public static void launch_secondary_jobtracker()
    {
    	MyUtils.info("A primary job tracker already exists. Launching secondary job tracker");
    	Watcher w = new Watcher() {
    		@Override
    		public void process(WatchedEvent event)
    		{
    			handle_watch_event(event);
    		}
    		
    	};
    	m_lock = new ReentrantLock();
    	m_cond = m_lock.newCondition();
    	boolean have_jobtracker_znode = false;
    	m_lock.lock();
    	try
    	{
    		do
    		{
    			if(m_zkc.exists(JOB_TRACKER_ZNODE, w) != null)
    			{
    				try
    				{
    					m_cond.await();
    				}
    				catch(InterruptedException e)
    				{
    					
    				}
    			}
    			Code ret = m_zkc.create(
    					JOB_TRACKER_ZNODE,
    					null,
                        CreateMode.EPHEMERAL
                        );
    			if(ret == Code.OK)
    			{
    				have_jobtracker_znode = true;
    			}
    			else if(ret == Code.NODEEXISTS)
    			{
    				MyUtils.info("Primary Tracker still exists. Remaining as secondary");
    			}
    			else
    			{
    				MyUtils.crit("Unknown status of znode on create: " + ret.name());
    			}
    		}
    		while(!have_jobtracker_znode);
    	}
    	finally
    	{
    		m_lock.unlock();
    	}
    }
	
	public static void main(String[] args)
	{
		MyUtils.process_non_listener_args(args);
		String host = MyUtils.opts.get("zookeeper");

		m_zkc = new ZkConnector();
        try 
        {
            m_zkc.connect(host);
        } 
        catch(Exception e) 
        {
            MyUtils.error("Failed to connect to Zookeeper "+ e.getMessage());
            System.exit(-1);
        }
        MyUtils.info("Connected to Zookeper at " + host);
        MyUtils.create_initial_persistent_znodes(m_zkc);
        
        Code ret = m_zkc.create(
        		JOB_TRACKER_ZNODE,
        		null,
                CreateMode.EPHEMERAL
                );
        if(ret == Code.OK)
        {
        	launch_primary_jobtracker();
        }
        else if(ret == Code.NODEEXISTS)
        {
        	launch_secondary_jobtracker();
        	// When the secondary jobtracker returns, it means the other primary has thrown the
        	// towel and we have grabbed the znode to become the new primary
        	launch_primary_jobtracker();
        }
        else
        {
        	MyUtils.info("Unknown Error code while attempting to create job tracker znode: " + ret.name());
        }

        try
        {
        	m_zkc.close();
        }
        catch(Exception e)
        {

        }
	}
	
	public static void handle_watch_event(WatchedEvent event)
	{
		m_lock.lock();
		try
		{
			m_cond.signal();
			MyUtils.debug("Received WatchedEvent: " + event.toString());
		}
		finally
		{
			m_lock.unlock();
		}
	}
	
	private static class ConnectionInfo
	{
		public String host;
		public int port;
	}

    private static class CircularWorkerList
    {
        private List<ConnectionInfo> m_list;
        private int m_next_idx = 0;
        
        public CircularWorkerList(List<ConnectionInfo> list)
        {
            m_list = list;
        }

        public ConnectionInfo next()
        {
            if(m_list == null || m_list.size() == 0)
            {
                return null;
            }
            if(m_next_idx >= m_list.size())
            {
                m_next_idx = 0;
            }
            ConnectionInfo ret = m_list.get(m_next_idx);
            m_next_idx = (m_next_idx + 1) % m_list.size();
            return ret;
        }
    }
}