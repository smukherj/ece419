import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.Watcher.Event.EventType;

import java.io.*;
import java.util.concurrent.locks.*;
import java.net.*;
import java.util.*;


public class FileServer extends Thread {

	private static Watcher m_watcher;
	private static ReentrantLock m_lock;
	private static Condition m_cond;
	public static final String FILE_SERVER_ZNODE = "/FileServer";
	private static ArrayList<String> m_dict;
	private static int m_num_partitions = 0;
	
	private ZkConnector m_zkc;
	private Socket m_sock;
	private ObjectInputStream m_istream;
	private ObjectOutputStream m_ostream;
	
	public FileServer(ZkConnector zkc, Socket sock)
	{
		m_zkc = zkc;
		m_sock = sock;
		m_istream = null;
		m_ostream = null;
		try
		{
			m_istream = new ObjectInputStream(sock.getInputStream());
			m_ostream = new ObjectOutputStream(sock.getOutputStream());
		}
		catch(Exception e)
		{
			MyUtils.error("Failed to establish incoming connection from " 
		+ sock.getInetAddress().getHostName() + ":" + sock.getPort());
		}
	}
	
	@Override
	public void run()
	{
		if(m_istream == null || m_ostream == null)
		{
			return;
		}
		try
		{
			MessagePacket m;
			while((m = (MessagePacket)m_istream.readObject()) != null)
			{
				if(m.type == MessagePacket.MessageType.GET_NUM_PARTITIONS)
				{
					m = new MessagePacket();
					m.type = MessagePacket.MessageType.PARTITION;
					m.num_elements = m_num_partitions;
					m_ostream.writeObject(m);
				}
				else if(m.type == MessagePacket.MessageType.GET_PARTITION)
				{
					if(m.partition >= m_num_partitions)
					{
						m = new MessagePacket();
						m.type = MessagePacket.MessageType.NO_SUCH_PARTITION;
						m_ostream.writeObject(m);
						continue;
					}
					int istart = m.partition * MyUtils.PARTITION_SIZE;
					int iend = istart + MyUtils.PARTITION_SIZE;
					int partition = m.partition;
					if(iend > m_dict.size())
					{
						iend = m_dict.size();
					}
					m = new MessagePacket();
					m.type = MessagePacket.MessageType.PARTITION_INFO;
					m.num_elements = iend - istart;
					m_ostream.writeObject(m);
					for(int i = istart; i < iend; ++i)
					{
						m = new MessagePacket();
						m.type = MessagePacket.MessageType.PARTITION;
						m.partition = partition;
						m.data = m_dict.get(i);
						m_ostream.writeObject(m);
					}
				}
				else
				{
					MyUtils.error("Unknown packet type " + m.type.name());
				}
			}
		}
		catch(Exception e)
		{
			//MyUtils.error("FileServer Thread Exception " + e.getMessage());
		}
	}
	
	public static void read_dictionary()
	{
		m_dict = new ArrayList<String>();
		FileReader f = null;
		BufferedReader br = null;
		try 
		{
			f = new FileReader(MyUtils.opts.get("dictfile"));
			br = new BufferedReader(f);
		} catch (Exception e) 
		{
			MyUtils.error("Dictionary file " + MyUtils.opts.get("dictfile") + " could not be opened");
			System.exit(-1);
		}
		try
		{
			String s;
			while((s = br.readLine()) != null)
			{
				m_dict.add(s);
			}
		}
		catch(Exception e)
		{
			MyUtils.error("Encountered an error while reading the dictionary file");
			MyUtils.error(e.getMessage());
			System.exit(-1);
		}
		if(m_dict.size() == 0)
		{
			m_num_partitions = 0;
		}
		else
		{
			m_num_partitions = (m_dict.size() / MyUtils.PARTITION_SIZE) + 1;
		}
		MyUtils.info("Read " + m_dict.size() + " words from dictionary (" + m_num_partitions + " partitions)");
	}

    public static void launch_primary_fileserver(ZkConnector zkc)
	{
    	MyUtils.attach_file_logger(new FileLogger("fileserver.log"));
		MyUtils.info("Launching primary File Server");
		ServerSocket socket = null;
        try
        {
        	socket = new ServerSocket(Integer.parseInt(MyUtils.opts.get("listenport")));
        }
        catch(Exception e)
        {
        	MyUtils.error("Failed to listen on port " + MyUtils.opts.get("listenport"));
        	e.printStackTrace();
        	System.exit(-1);
        }
        MyUtils.info("Primary server available at " + MyUtils.opts.get("hostaddr") + ":" + MyUtils.opts.get("listenport"));
        Socket clientsock;
        while(true)
        {
        	try
        	{
        		while((clientsock = socket.accept()) != null)
        		{
        			new FileServer(zkc, clientsock).start();
        		}
        	}
        	catch(Exception e)
        	{
        		// ignore failed connection attempts
        	}
        }
	}
    
    public static void launch_secondary_fileserver(ZkConnector zkc)
    {
    	MyUtils.info("A primary File Server already exists. Launching secondary File Server");
    	Watcher w = new Watcher() {
    		@Override
    		public void process(WatchedEvent event)
    		{
    			handle_watch_event(event);
    		}
    		
    	};
    	m_lock = new ReentrantLock();
    	m_cond = m_lock.newCondition();
    	boolean have_jobtracker_znode = false;
    	m_lock.lock();
    	try
    	{
    		do
    		{
    			if(zkc.exists(FILE_SERVER_ZNODE, w) != null)
    			{
    				try
    				{
    					m_cond.await();
    				}
    				catch(InterruptedException e)
    				{
    					
    				}
    			}
    			Code ret = zkc.create(
    					FILE_SERVER_ZNODE,
                        MyUtils.opts.get("hostaddr") + "," + MyUtils.opts.get("listenport"),
                        CreateMode.EPHEMERAL
                        );
    			if(ret == Code.OK)
    			{
    				have_jobtracker_znode = true;
    			}
    			else if(ret == Code.NODEEXISTS)
    			{
    				MyUtils.info("Primary Server still exists. Remaining as secondary");
    			}
    			else
    			{
    				MyUtils.crit("Unknown status of znode on create: " + ret.name());
    			}
    		}
    		while(!have_jobtracker_znode);
    	}
    	finally
    	{
    		m_lock.unlock();
    	}
    }
	
	public static void main(String[] args)
	{
		MyUtils.process_args(args);
		MyUtils.opts.get_or_die("dictfile");
		String host = MyUtils.opts.get("zookeeper");
		read_dictionary();
		ZkConnector zkc = new ZkConnector();
        try 
        {
            zkc.connect(host);
        } 
        catch(Exception e) 
        {
            MyUtils.error("Failed to connect to Zookeeper "+ e.getMessage());
            System.exit(-1);
        }
        MyUtils.info("Connected to Zookeper at " + host);
        MyUtils.create_initial_persistent_znodes(zkc);
        
        Code ret = zkc.create(
        		FILE_SERVER_ZNODE,
        		MyUtils.opts.get("hostaddr") + "," + MyUtils.opts.get("listenport"),
                CreateMode.EPHEMERAL
                );
        if(ret == Code.OK)
        {
        	launch_primary_fileserver(zkc);
        }
        else if(ret == Code.NODEEXISTS)
        {
        	launch_secondary_fileserver(zkc);
        	// When the secondary jobtracker returns, it means the other primary has thrown the
        	// towel and we have grabbed the znode to become the new primary
        	launch_primary_fileserver(zkc);
        }
        else
        {
        	MyUtils.info("Unknown Error code while attempting to create File Server znode: " + ret.name());
        }

        try
        {
        	zkc.close();
        }
        catch(Exception e)
        {

        }
	}
	
	public static void handle_watch_event(WatchedEvent event)
	{
		m_lock.lock();
		try
		{
			m_cond.signal();
			MyUtils.debug("Received WatchedEvent: " + event.toString());
		}
		finally
		{
			m_lock.unlock();
		}
	}

}
