import java.io.*;


public class MessagePacket implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6088720188604941371L;

	MessageType type;
	
	String job = null;
	String data = null;
	int partition = -1;
	int num_elements = -1;
	
	public enum MessageType
	{
		SUBMIT_JOB,
		QUERY_JOB,
		GET_PARTITION,
		GET_NUM_PARTITIONS,
		PARTITION_INFO,
		PARTITION,
		NO_SUCH_PARTITION
	}
}
