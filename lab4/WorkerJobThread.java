import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;

public class WorkerJobThread extends Thread {
	private ZkConnector m_zkc;
	private String m_job_password;
	private int m_partitionid;
	private String m_job_desc;
	
	private static final int MAX_ACTIVE_WORKERS = 4;
	private static Integer m_num_active_workers = new Integer(0);
	
	private List<String> m_partition;
	
	public WorkerJobThread(ZkConnector zkc, String job_password, int partition)
	{
		m_zkc = zkc;
		m_job_password = job_password;
		m_partitionid = partition;
		m_partition = null;
	}
	
	private void obtain_partition() throws Exception
	{
		MyUtils.debug("Obtaining partition for " + m_job_desc);
		m_partition = Worker.get_cached_partition(m_partitionid);
		if(m_partition != null)
		{
			// Found partition in cache
			return;
		}
		m_partition = new ArrayList<String>();
		
		String data = new String(m_zkc.getZooKeeper().getData(FileServer.FILE_SERVER_ZNODE, false, null));
		String hostname = "<unknown>";
		int port = -1;
		
		try
		{
			hostname = MyUtils.parseHost(data);
			port = MyUtils.parsePort(data);
		}
		catch(Exception e)
		{
			MyUtils.error("Failed to parse FileServer connection information");
			throw e;
		}
		
		Socket socket = null;
		ObjectOutputStream ostream = null;
		ObjectInputStream istream = null;
		try
		{
			socket = new Socket(hostname, port);
			ostream = new ObjectOutputStream(socket.getOutputStream());
			istream = new ObjectInputStream(socket.getInputStream());
		}
		catch(Exception e)
		{
			MyUtils.error("Failed to setup connection to fileserver at " + hostname + ":" + port);
			throw e;
		}
		MessagePacket m = new MessagePacket();
		m.type = MessagePacket.MessageType.GET_PARTITION;
		m.partition = m_partitionid;
		ostream.writeObject(m);
		m = (MessagePacket)istream.readObject();
		if(m.type == MessagePacket.MessageType.NO_SUCH_PARTITION)
		{
			throw new Exception("Partition not found");
		}
		else if(m.type != MessagePacket.MessageType.PARTITION_INFO)
		{
			throw new Exception("FileServer replied with unrecognized packet type " + m.type.name());
		}
		MyUtils.debug("FileServer has partition with " + m.num_elements + " elements");
		int num_elements = m.num_elements;
		for(int i = 0; i < num_elements; ++i)
		{
			m = (MessagePacket)istream.readObject();
			m_partition.add(m.data);
			if(m.type != MessagePacket.MessageType.PARTITION)
			{
				throw new Exception("FileServer replied with unexpected packet type " + m.type.name());
			}
			MyUtils.vdebug("Received element " + Integer.toString(i + 1) +" '" + m.data + "'");
		}
		MyUtils.debug("Finished receiving partition for " + m_job_desc);
		Worker.cache_partition(m_partitionid, m_partition);
		ostream.flush();
		ostream.close();
		istream.close();
		socket.close();
	}
	
	private JobStatus run_job() throws Exception
	{
		JobStatus j = new JobStatus();
		obtain_partition();
		for(String dict_word : m_partition)
		{
			String hash = MyUtils.getHash(dict_word);
			if(hash.equals(m_job_password))
			{
				j.cracked_password = new String(dict_word);
				MyUtils.debug("Cracked password in partition " + m_partitionid + ". Password is " + dict_word);
				break;
			}
		}
		j.type = JobStatus.StatusType.DONE;
		j.password = new String(m_job_password);
		return j;
	}
	
	@Override
	public void run()
	{
		String work_progress_znode = null;
		synchronized(m_num_active_workers)
		{
			if(m_num_active_workers >= MAX_ACTIVE_WORKERS)
			{
				return;
			}
			++m_num_active_workers;
		}
		try
		{
			m_job_desc = "Job " + m_job_password;
			m_job_desc += ", Partition " + Integer.toString(m_partitionid);
			if(m_job_password ==  null || m_partitionid < 0)
			{
				MyUtils.warn("Bad job request : " + m_job_desc);
				return;
			}
			work_progress_znode = MyUtils.JOBS_ZNODE + "/" + m_job_password +
					"/" + Integer.toString(m_partitionid) + "P";
			String work_done_znode = MyUtils.JOBS_ZNODE + "/" + m_job_password +
					"/" + Integer.toString(m_partitionid) + "D";
			
			MyUtils.debug("Started processing " + m_job_desc);
			m_zkc.create(work_progress_znode, null, CreateMode.EPHEMERAL);
			JobStatus j = run_job();
			m_zkc.create(work_done_znode, j.toString(), CreateMode.PERSISTENT);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			MyUtils.error(e.getMessage() + " while processing " + m_job_desc);
		}
		finally
		{
			synchronized(m_num_active_workers)
			{
				m_num_active_workers--;
			}
			try 
			{
				m_zkc.getZooKeeper().delete(work_progress_znode, -1);
			} 
			catch (Exception e1) 
			{
			}
			MyUtils.debug("Finished processing " + m_job_desc);
		}
	}
}
