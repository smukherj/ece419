import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.Watcher.Event.EventType;

import java.io.*;
import java.util.concurrent.locks.*;
import java.net.*;
import java.util.*;

public class Worker {
	
	private static List<List<String>> m_cached_partitions;
	
	private ZkConnector m_zkc;
	private Socket m_sock;
	
	public static List<String> get_cached_partition(int partition)
	{
		List<String> result = null;
		
		// Is caching turned on?
		if(MyUtils.opts.get("cache") == null)
		{
			return null;
		}
		
		synchronized(m_cached_partitions)
		{
			if(m_cached_partitions.size() > partition)
			{
				result = m_cached_partitions.get(partition);
			}
		}
		return result;
	}
	
	public static void cache_partition(int pid, List<String> partition)
	{
		// Is caching turned on?
		if(MyUtils.opts.get("cache") == null)
		{
			return;
		}
		
		synchronized(m_cached_partitions)
		{
			while(m_cached_partitions.size() <= pid)
			{
				m_cached_partitions.add(null);
			}
			if(m_cached_partitions.get(pid) == null)
			{
				m_cached_partitions.set(pid, partition);
			}
		}
	}
	
	public static void run(ZkConnector zkc, Socket sock) throws Exception
	{
		try
		{
			ObjectInputStream istream = new ObjectInputStream(sock.getInputStream());
			MessagePacket m= (MessagePacket)istream.readObject();
			if(m.type == MessagePacket.MessageType.SUBMIT_JOB)
			{
				new WorkerJobThread(zkc, m.job, m.partition).start();
			}
			else
			{
				MyUtils.warn("Invalid message of type " + m.type.name());
			}
			istream.close();
		}
		catch(Exception e)
		{
			//MyUtils.error("Worker Thread Exception " + e.getMessage());
		}
		sock.close();
	}
	
	private static void create_worker_ephemeral_znode(ZkConnector zkc)
	{
		for(int i = 0; i < MyUtils.CLUSTER_SIZE; ++i)
		{
			Code ret = zkc.create(MyUtils.WORKERS_ZNODE + "/" + Integer.toString(i),
					MyUtils.opts.get("hostaddr") + "," + MyUtils.opts.get("listenport"),
	                CreateMode.EPHEMERAL
	                );
			if(ret == Code.OK)
			{
				// Success. We claimed worker id 'i'
				MyUtils.attach_file_logger(new FileLogger("worker_" + Integer.toString(i) + ".log"));
				return;
			}
		}
		MyUtils.error("Number of active workers has exceed maximum cluster size " + MyUtils.CLUSTER_SIZE);
		MyUtils.error("Please adjust CLUSTER_SIZE in MyUtils.java, recompile and run");
		System.exit(-1);
	}
	
	public static void main(String []args)
	{
		MyUtils.process_args(args);
		String host = MyUtils.opts.get("zookeeper");
		m_cached_partitions = new ArrayList<List<String>>();

		ZkConnector zkc = new ZkConnector();
        try 
        {
            zkc.connect(host);
        } 
        catch(Exception e) 
        {
            MyUtils.error("Failed to connect to Zookeeper "+ e.getMessage());
            System.exit(-1);
        }
        MyUtils.info("Connected to Zookeper at " + host);
        MyUtils.create_initial_persistent_znodes(zkc);
        
        create_worker_ephemeral_znode(zkc);
        MyUtils.info("Worker is available at " + MyUtils.opts.get("hostaddr") + ":" + MyUtils.opts.get("listenport"));
        
        ServerSocket socket = null;
        try
        {
        	socket = new ServerSocket(Integer.parseInt(MyUtils.opts.get("listenport")));
        }
        catch(Exception e)
        {
        	MyUtils.error("Failed to listen on port " + MyUtils.opts.get("listenport"));
        	e.printStackTrace();
        	System.exit(-1);
        }
        Socket clientsock;
        while(true)
        {
        	try
        	{
        		while((clientsock = socket.accept()) != null)
        		{
        			run(zkc, clientsock);
        		}
        	}
        	catch(Exception e)
        	{
        		// ignore failed connection attempts
        	}
        }
	}
}
