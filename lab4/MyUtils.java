import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.math.BigInteger;

import org.apache.zookeeper.CreateMode;
import java.io.*;
import java.util.*;

public class MyUtils {
    public static int logLevel = 0;
    private static FileLogger m_file_logger = null;
    public static ArgumentParser opts = null;
    public static final String JOBS_ZNODE = "/Jobs";
    public static final String FINISHED_JOBS_ZNODE = "/FinishedJobs";
    public static final String WORKERS_ZNODE = "/Workers";
    public static final int CLUSTER_SIZE = 100;
    public static final int PARTITION_SIZE = 100000;
    
    public static void attach_file_logger(FileLogger f)
    {
    	m_file_logger = f;
    }
    
    public static void process_non_listener_args(String []args)
    {
    	MyUtils.opts = new ArgumentParser(args);
		MyUtils.opts.get_or_die("zookeeper");

		if(MyUtils.opts.get("vdebug") != null)
		{
			MyUtils.logLevel = 3;
		}
		else if(MyUtils.opts.get("debug") != null)
		{
			MyUtils.logLevel = 2;
		}
		else if(MyUtils.opts.get("quiet") != null)
		{
			MyUtils.logLevel = 0;
		}
		else
		{
			MyUtils.logLevel = 1;
		}
    }
    
    public static void process_args(String []args)
    {
    	MyUtils.opts = new ArgumentParser(args);
		MyUtils.opts.get_or_die("zookeeper");
		MyUtils.opts.get_or_die("hostaddr");
		String port = MyUtils.opts.get_or_die("listenport");
		try
		{
			Integer.parseInt(port);
		}
		catch(NumberFormatException e)
		{
			MyUtils.error("Listenport was not a valid port");
		}

		if(MyUtils.opts.get("vdebug") != null)
		{
			MyUtils.logLevel = 3;
		}
		else if(MyUtils.opts.get("debug") != null)
		{
			MyUtils.logLevel = 2;
		}
		else if(MyUtils.opts.get("quiet") != null)
		{
			MyUtils.logLevel = 0;
		}
		else
		{
			MyUtils.logLevel = 1;
		}
    }
    
    public static void create_initial_persistent_znodes(ZkConnector zkc)
    {
    	zkc.create(JOBS_ZNODE, null, CreateMode.PERSISTENT);
    	zkc.create(FINISHED_JOBS_ZNODE, null, CreateMode.PERSISTENT);
    	zkc.create(WORKERS_ZNODE, null, CreateMode.PERSISTENT);
    }

    public static String getHash(String word) {

        String hash = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            BigInteger hashint = new BigInteger(1, md5.digest(word.getBytes()));
            hash = hashint.toString(16);
            while (hash.length() < 32) hash = "0" + hash;
        } catch (NoSuchAlgorithmException nsae) {
            // ignore
        }
        return hash;
    }

    private static void log(int level, String s)
    {
        if(level <= MyUtils.logLevel)
        {
        	if(m_file_logger != null)
        	{
        		m_file_logger.log(s);
        	}
            System.out.println(s);
        }
    }

    public static void info(String s)
    {
        log(1, "Info: " + s);
    }
    
    public static void display(String s)
    {
    	log(logLevel, "Info: " + s);
    }
    
    public static void warn(String s)
    {
        log(1, "Warning: " + s);
    }

    public static void crit(String s)
    {
        log(0, "Critical: " + s);
    }

    public static void error(String s)
    {
        log(0, "Error: " + s);
    }

    public static void debug(String s)
    {
        log(2, "Debug: " + s);
    }
    
    public static void vdebug(String s)
    {
        log(3, "Debug: " + s);
    }
    
    public static String parseHost(String s)
    {
    	return s.split(",")[0].trim();
    }
    
    public static int parsePort(String s)
    {
    	return Integer.parseInt(s.split(",")[1].trim());
    }
}
