import hashlib
from random import shuffle

word_list = []
for line in open('lowercase.rand'):
	word_list.append(line.strip())
shuffle(word_list)

fout = open('create_jobs.txt', 'w')
for i in range(1000):
	md5 = hashlib.md5()
	md5.update(word_list[i])
	fout.write('s ' + md5.hexdigest() + '\n')