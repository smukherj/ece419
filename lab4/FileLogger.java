import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FileLogger {
	private FileWriter logfile = null;
	private DateFormat dateFormat = null;
	
	public FileLogger(String filename)
	{
		try
		{
			logfile = new FileWriter(filename, true);
		}
		catch(IOException e)
		{
			MyUtils.warn("Could not create the file logger: " + e.getMessage());
			logfile = null;
		}
		dateFormat = new SimpleDateFormat("HH:mm:ss");
	}
	
	public synchronized void log(String s)
	{
		if(logfile != null)
		{
			try
			{
				String timestamp = "[" + dateFormat.format(Calendar.getInstance().getTime()) + "]";
				logfile.write(timestamp + " " + s + "\n");
				logfile.flush();
			}
			catch(IOException e)
			{
				
			}
		}
	}
}
