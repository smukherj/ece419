import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgumentParser {
	private HashMap<String, String> m_opts;
	
	public ArgumentParser(String []args)
	{
		m_opts = new HashMap<String, String>();
		Pattern argvalpattern = Pattern.compile("--?([^=]+)=(.+)");
		Pattern argpattern = Pattern.compile("--?(.+)");
		for(String arg : args)
		{
			Matcher m = argvalpattern.matcher(arg);
			if(m.find())
			{
				m_opts.put(m.group(1), m.group(2));
				continue;
			}
			
			m = argpattern.matcher(arg);
			if(m.find())
			{
				m_opts.put(m.group(1), "true");
				continue;
			}
		}
	}
	
	public String get_or_die(String arg)
	{
		String val = m_opts.get(arg);
		if(val == null)
		{
			MyUtils.error("Command line argument --" + arg + " was not provided");
			System.exit(-1);
		}
		return val;
	}
	
	public String get(String arg)
	{
		return m_opts.get(arg);
	}
}
