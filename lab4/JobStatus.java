

public class JobStatus {

	/**
	 * 
	 */
	private static final long serialVersionUID = -847729548700377642L;
	public enum StatusType
	{
		READY,
		INPROGRESS,
		DONE
	}
	public StatusType type = StatusType.READY;
	public String password = null;
	public String cracked_password = null;
	
	public String toString()
	{
		String pass = password == null ? "null" : password;
		String cpass = cracked_password == null ? "null" : cracked_password;
		return type.name() + "\n" + pass + "\n" + cpass;
	}
	
	public static JobStatus parseString(String s)
	{
		if(s == null)
		{
			return null;
		}
		JobStatus j = new JobStatus();
		String []split_s = s.split("\n");
		if(split_s.length != 3)
		{
			return null;
		}
		
		j.type = StatusType.valueOf(split_s[0]);
		if(split_s[1].equals("null"))
		{
			j.password = null;
		}
		else
		{
			j.password = split_s[1];
		}
		if(split_s[2].equals("null"))
		{
			j.cracked_password = null;
		}
		else
		{
			j.cracked_password = split_s[2];
		}
		return j;
	}
}
