import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;
import org.apache.zookeeper.Watcher.Event.EventType;

import java.io.*;
import java.util.concurrent.locks.*;
import java.net.*;
import java.util.*;

public class ClientDriver {
	
	private static ZkConnector m_zkc;
	
	private static void create_job(String password)
	{
		JobStatus j = new JobStatus();
		j.type = JobStatus.StatusType.READY;
		j.password = password.toLowerCase();
		String job = j.toString();
		Code ret = m_zkc.create(MyUtils.JOBS_ZNODE + "/" + j.password,
				job,
				CreateMode.PERSISTENT
				);
		if(ret == Code.OK)
		{
			MyUtils.display("Submitted job for hash '" + j.password + "'");
		}
		else if(ret == Code.NODEEXISTS)
		{
			MyUtils.display("Job for " + j.password + " already exists");
		}
		else
		{
			MyUtils.error("Encountered " + ret.name() + " while submitting job");
		}
	}
	
	private static void query_job(String password)
	{
		String path = MyUtils.JOBS_ZNODE + "/" + password;
		try
		{
			String data = new String(m_zkc.getZooKeeper().getData(path, false, null));
			JobStatus j = JobStatus.parseString(data);
			if(j != null)
			{
				MyUtils.display("-------------------------------------------------------------");
				MyUtils.display("    Password Hash: " + j.password);
				MyUtils.display("    Job Status: " + j.type.name());
				if(j.cracked_password == null)
				{
					MyUtils.display("    Password not cracked");
				}
				else
				{
					MyUtils.display("    Cracked password: " + j.cracked_password);
				}
				MyUtils.display("-------------------------------------------------------------");
			}
		}
		catch(Exception e)
		{
			MyUtils.display("Job not found");
		}
	}
	
	public static void list_jobs()
	{
		String path = MyUtils.JOBS_ZNODE;
		try 
		{
			List<String> jobs = m_zkc.getZooKeeper().getChildren(path, false);
			MyUtils.display("Found " + jobs.size() + " jobs");
			for(String job : jobs)
			{
				query_job(job);
			}
		} 
		catch (Exception e) 
		{
			MyUtils.error("Encountered exception '" + e.getMessage() + "'");
		}
	}
	
	public static void display_help()
	{
		MyUtils.display("Enter 'q <password>' to query the status of the job for the given password");
		MyUtils.display("Enter 's <password>' to submit a job with the given password");
		MyUtils.display("Enter 'l' to list the status of every job ever submitted");
	}
	
	public static void process_args(String []args)
	{
		String cmd = args[0].toLowerCase();
		try
		{
			if(cmd.equals("l"))
			{
				list_jobs();
			}
			else if(cmd.equals("q"))
			{
				query_job(args[1]);
			}
			else if(cmd.equals("s"))
			{
				create_job(args[1]);
			}
		}
		catch(IndexOutOfBoundsException e)
		{
			MyUtils.error("Not enough arguments for given command");
		}
	}
	
	public static void run_prompt(Reader r)
	{
		BufferedReader br = new BufferedReader(r);
		String line;
		try 
		{
			System.out.print("> ");
			while((line = br.readLine()) != null)
			{
				String[] args = line.split("[ \t]");
				if(args.length == 0)
				{
					continue;
				}
				process_args(args);
				System.out.print("> ");
			}
		} 
		catch (IOException e) 
		{
			MyUtils.error("Encountered '" + e.getMessage() + "' while reading from stdin");
		}
	}
	
	public static void main(String []args)
	{
		MyUtils.opts = new ArgumentParser(args);
		MyUtils.opts.get_or_die("zookeeper");
		String host = MyUtils.opts.get("zookeeper");

		m_zkc = new ZkConnector();
        try 
        {
            m_zkc.connect(host);
        } 
        catch(Exception e) 
        {
            MyUtils.error("Failed to connect to Zookeeper "+ e.getMessage());
            System.exit(-1);
        }
        MyUtils.info("Connected to Zookeper at " + host);
        MyUtils.create_initial_persistent_znodes(m_zkc);
        display_help();
        
        if(MyUtils.opts.get("file") != null)
        {
        	String filename = MyUtils.opts.get("file");
        	try
        	{
        		FileReader f = new FileReader(filename);
        		run_prompt(f);
        	}
        	catch(Exception e)
        	{
        		MyUtils.error("Could not open '" + filename + "' to read commands from");
        	}
        }
        else
        {
        	run_prompt(new InputStreamReader(System.in));
        }
        
        try
        {
        	m_zkc.close();
        }
        catch(Exception e)
        {

        }
	}
}
