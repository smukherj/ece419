import java.lang.Thread;

public class MazeImplDequeueThread extends Thread {

	private MazeImpl maze = null;

	public MazeImplDequeueThread(MazeImpl maze) {
		super("MazeImplDequeueThread");
		assert(maze != null);
		this.maze = maze;
	}

	public void run() {
		while(!isInterrupted()) {
			MessagePacket nextPacket = null;
			Client client;

			nextPacket = maze.dequeue();
			assert(nextPacket != null);

			switch(nextPacket.type){
			case MessagePacket.ERROR_UNKNOWN:
				System.out.println("Warning: MazeImplDequeueThread: Error packet receieved from server.");
				break;
			case MessagePacket.SERVER_SPAWN:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					// When the local client is being spawned for the first time,
					// it wouldn't have been registered in the client map
					if(nextPacket.client_name.equals(maze.getLocalClient().getName()))
					{
						client = maze.getLocalClient();
					}
					else
					{
						client = new RemoteClient(nextPacket.client_name);
						maze.addClient(client);
					}
				}
				maze.spawnClient(client, nextPacket.point, nextPacket.dir);
				break;
			case MessagePacket.SERVER_UP:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.forward();
				}
				break;
			case MessagePacket.SERVER_DOWN:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.backup();
				}
				break;
			case MessagePacket.SERVER_RIGHT:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.turnRight();
				}
				break;
			case MessagePacket.SERVER_LEFT:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.turnLeft();
				}
				break;
			case MessagePacket.SERVER_SPACE:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.fire();
				}
				break;
			case MessagePacket.SERVER_EXIT:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					if(nextPacket.error_code == MessagePacket.ERROR_DUPL_USER)
					{
						System.out.println("Error: Failed to register as " + nextPacket.client_name + "." + 
								" Username already taken.");
						maze.cleanup();
						Mazewar.quit();
						break;
					}
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					if(client instanceof RemoteClient){
						maze.removeClient(client);
					}
					else if(client instanceof LocalClient){
						maze.cleanup();
						Mazewar.quit();
					} else{
						System.out.println("Warning: MazeImplDequeueThread: client type not supported.");
					}
				}
				break;
			case MessagePacket.SERVER_TICK:
				maze.handleProjectiles();
				break;
			default:
				System.out.println("Warning: MazeImplDequeueThread: MessagePacket type not supported.");
				break;
			}
		}
	}
}
