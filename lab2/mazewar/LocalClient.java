/*
Copyright (C) 2004 Geoffrey Alan Washburn
      
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
      
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
      
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
*/

import java.io.*;
import java.net.*;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

public class LocalClient extends Client implements KeyListener {

        /**
         * Create a LocalClient. 
         */
        public LocalClient(String name) {
                super(name);
		assert(name != null);
        }

	/* Register LocalClient with Mazewar server */
	public void spawn(){
		MessagePacket spawnPacket = new MessagePacket();

		spawnPacket.type = MessagePacket.CLIENT_SPAWN;
		spawnPacket.client_name = getName();
		spawnPacket.point = maze.getSpawnPoint();
		spawnPacket.dir = maze.getSpawnDirection(spawnPacket.point);

		try{	
			maze.sendToServer(spawnPacket);
		} catch (IOException e){
			System.out.println("Warning: LocalClient: Couldn't send spawn packet to Mazewar server.");
			e.printStackTrace();
		} catch (ClassNotFoundException e){
			System.out.println("Warning: LocalClient: ClassNotFoundException while spawning.");
		}
	}
        
        /**
         * Handle a key press.
         * @param e The {@link KeyEvent} that occurred.
         */
        public void keyPressed(KeyEvent e) {
                MessagePacket packetToServer = new MessagePacket();

                packetToServer.client_name = getName();

		// If the user pressed Q, invoke the cleanup code and quit. 
                if((e.getKeyChar() == 'q') || (e.getKeyChar() == 'Q')) {
			packetToServer.type = MessagePacket.CLIENT_EXIT;
                        try{
                                maze.sendToServer(packetToServer);
                        } catch (IOException ex){
                                System.out.println("Warning: LocalClient: Couldn't send packet to Mazewar server.");
                        } catch (ClassNotFoundException ex){
                                System.out.println("Warning: LocalClient: ClassNotFoundException while sending packet.");
                        }
                // Up-arrow moves forward.
                } else if(e.getKeyCode() == KeyEvent.VK_UP) {
                        packetToServer.type = MessagePacket.CLIENT_UP;
                        try{
                                maze.sendToServer(packetToServer);
                        } catch (IOException ex){
                                System.out.println("Warning: LocalClient: Couldn't send packet to Mazewar server.");
                        } catch (ClassNotFoundException ex){
                                System.out.println("Warning: LocalClient: ClassNotFoundException while sending packet.");
                        }
                // Down-arrow moves backward.
                } else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			packetToServer.type = MessagePacket.CLIENT_DOWN;
                        try{
                                maze.sendToServer(packetToServer);
                        } catch (IOException ex){
                                System.out.println("Warning: LocalClient: Couldn't send packet to Mazewar server.");
                        } catch (ClassNotFoundException ex){
                                System.out.println("Warning: LocalClient: ClassNotFoundException while sending packet.");
                        }
                // Left-arrow turns left.
                } else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			packetToServer.type = MessagePacket.CLIENT_LEFT;
                        try{
                                maze.sendToServer(packetToServer);
                        } catch (IOException ex){
                                System.out.println("Warning: LocalClient: Couldn't send packet to Mazewar server.");
                        } catch (ClassNotFoundException ex){
                                System.out.println("Warning: LocalClient: ClassNotFoundException while sending packet.");
                        }
                // Right-arrow turns right.
                } else if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			packetToServer.type = MessagePacket.CLIENT_RIGHT;
                        try{
                                maze.sendToServer(packetToServer);
                        } catch (IOException ex){
                                System.out.println("Warning: LocalClient: Couldn't send packet to Mazewar server.");
                        } catch (ClassNotFoundException ex){
                                System.out.println("Warning: LocalClient: ClassNotFoundException while sending packet.");
                        }
                // Spacebar fires.
                } else if(e.getKeyCode() == KeyEvent.VK_SPACE) {
                        packetToServer.type = MessagePacket.CLIENT_SPACE;
			try{
                        	maze.sendToServer(packetToServer);
                	} catch (IOException ex){
                        	System.out.println("Warning: LocalClient: Couldn't send packet to Mazewar server.");
                	} catch (ClassNotFoundException ex){
                        	System.out.println("Warning: LocalClient: ClassNotFoundException while sending packet.");
                	}
                }
        }
        
        /**
         * Handle a key release.
         * @param e The {@link KeyEvent} that occurred.
         */
        public void keyReleased(KeyEvent e) {
        }
        
        /**
         * Handle a key being typed.
         * @param e The {@link KeyEvent} that occurred.
         */
        public void keyTyped(KeyEvent e) {
        }

}
