import java.io.Serializable;
 /**
 * MessagePacket
 * ============
 * 
 * Packet format of the packets exchanged between the Client(s) and the Server
 * 
 */

public class MessagePacket implements Serializable {

	/* client -> server */
	public static final int CLIENT_SPAWN	= 100; /* spawn */
	public static final int CLIENT_UP	= 101; /* move forward */
	public static final int CLIENT_DOWN	= 102; /* move backward */
	public static final int CLIENT_RIGHT	= 103; /* rotate clockwise */
	public static final int CLIENT_LEFT	= 104; /* rotate counter-clockwise */
	public static final int CLIENT_SPACE 	= 105; /* fire a bullet */
	public static final int CLIENT_EXIT     = 106; /* exit/quit the game */
	
	/* server -> client */
	public static final int SERVER_SPAWN		= 200; /* spawn request */
	public static final int SERVER_UP       	= 201; /* move forward */
	public static final int SERVER_DOWN     	= 202; /* move backward */
	public static final int SERVER_RIGHT   		= 203; /* rotate clockwise */
	public static final int SERVER_LEFT     	= 204; /* rotate counter-clockwise */
	public static final int SERVER_SPACE    	= 205; /* fire a bullet */
	public static final int SERVER_EXIT     	= 206; /* exit/quit the game */
	public static final int SERVER_TICK             = 207; /* bullet tick */
		
	/* error codes */
	public static final int ERROR_UNKNOWN	= -101; /* unknown error */
	public static final int ERROR_DUPL_USER = -102; /* username already taken */
	
	public int type;	
	public String client_name;
	public Point point;
	public Direction dir;
	
	public int error_code;
	
	public MessagePacket()
	{
		type = -1;
		client_name = null;
		point = null;
		dir = null;
	}
	/*
	 * Copy constructor
	 */
	public MessagePacket(MessagePacket m)
	{
		type = m.type;
		client_name = new String(m.client_name);
		if(m.point == null)
		{
			point = null;
			dir = null;
		}
		else
		{
			point = new Point(m.point);
			dir = new Direction(m.dir);
		}
		error_code = m.error_code;
	}
}
