import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

public class MazewarServer {

	public MazewarServer()
	{
		m_client_list = new ArrayList<ServerClient>();
		m_packet_queue = new ArrayList<MessagePacket>();
		m_packet_queue_not_full = m_packet_queue_lock.newCondition();
	}
	
	public void add_client(ServerClient sc) throws IOException
	{
		m_client_list_lock.lock();
		try
		{
			for(ServerClient isc : m_client_list)
			{
				if(isc.name.equals(sc.name))
				{
					sc.clientinstream.close();
					sc.clientoutstream.close();
					sc.clientsock.close();
					throw new IOException("Rejecting client connection due to duplicate username");
				}
			}
			m_client_list.add(sc);
		}
		finally
		{
			m_client_list_lock.unlock();
		}
	}
	
	public boolean keep_broadcasting()
	{
		return m_keep_broadcasting;
	}
	
	public boolean remove_client(ServerClient sc)
	{
		m_client_list_lock.lock();
		boolean result = false;
		try
		{
			result = m_client_list.remove(sc);
			if(m_client_list.isEmpty())
			{
				m_keep_broadcasting = false;
			}
		}
		finally
		{
			m_client_list_lock.unlock();
		}
		return result;
	}

	
	public void run_hello_protocol(ServerClient sc) throws ClassNotFoundException, IOException
	{
		// Bad idea to run this in the server thread. A malicious client
		// can just connect and keep quiet and deny service to everyone else
		// but this will be run in a trusted environment and I'm feeling
		// lazy...
		MessagePacket m = (MessagePacket) sc.clientinstream.readObject();
		if(m.type != MessagePacket.CLIENT_SPAWN || m.client_name.length() < 1)
		{
			m = new MessagePacket();
			m.type = MessagePacket.ERROR_UNKNOWN;
			sc.clientoutstream.writeObject(m);
			throw new IOException("Initialization protocol error");
		}
		sc.name = m.client_name;
		sc.initial_point = m.point;
		sc.initial_direction = m.dir;
	}
	
	public ServerClient create_ServerClient(Socket clientsock) throws IOException, ClassNotFoundException
	{
		ServerClient sc = new ServerClient();
		sc.clientsock = clientsock;
		sc.clientoutstream = new ObjectOutputStream(clientsock.getOutputStream());
		sc.clientinstream = new ObjectInputStream(clientsock.getInputStream());
		run_hello_protocol(sc);
		add_client(sc);
		return sc;
	}
	
	public void enqueue_packet(MessagePacket m)
	{
		m_packet_queue_lock.lock();
		try
		{
			m_packet_queue.add(m);
			m_packet_queue_not_full.signal();
		}
		finally
		{
			m_packet_queue_lock.unlock();
		}
	}
	
	public MessagePacket dequeue_packet()
	{
		MessagePacket m = null;
		m_packet_queue_lock.lock();
		try
		{
			while(m_packet_queue.isEmpty())
			{
				try
				{
					m_packet_queue_not_full.await();
				}
				catch(InterruptedException e)
				{
					// whateva...
				}
			}
			m = m_packet_queue.remove(0);
		}
		finally
		{
			m_packet_queue_lock.unlock();
		}
		return m;
	}
	
	public void broadcast(MessagePacket m) throws IOException
	{
		m_client_list_lock.lock();
		try
		{
			for(ServerClient s : m_client_list)
			{
				s.clientoutstream.writeObject(m);
			}
		}
		finally
		{
			m_client_list_lock.unlock();
		}
	}
	
	public void run(int port)
	{
		System.out.println("Info: Attempting to start Mazewar server on localhost:" + Integer.toString(port));
		ServerSocket socket = null;
		try
		{
			socket = new ServerSocket(port);
		}
		catch(IOException e)
		{
			System.err.println("Error: Failed to start server. Got IO exception");
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
		catch(IllegalArgumentException e)
		{
			System.err.println("Error: Failed to start server. Got IllegalArgument exception");
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Info: Server initialization was successful.");
		System.out.println("Info: Waiting for clients to join...");
		int num_clients = 0;
		while(num_clients != 4)
		{
			try
			{
				Socket clientsock = socket.accept();
				create_ServerClient(clientsock);
				num_clients++;
				System.out.println("Info: Client " + num_clients + " has connected");
			}
			catch(IOException e)
			{
				System.out.println("Warning: Failed connection attempt from a client (IOException)");
				e.printStackTrace();
			}
			catch(ClassNotFoundException e)
			{
				System.out.println("Warning: Failed connection attempt from a client (ClassNotFoundException");
				e.printStackTrace();
			}
			catch(NullPointerException e)
			{
				System.out.println("Warning: Failed connection attempt from a client (NullPointerException)");
				e.printStackTrace();
			}
		}
		
		try
		{
			socket.close();
		}
		catch(IOException e)
		{
			
		}
		
		System.out.println("Info: " + num_clients + " are online. Starting game...");
		for(ServerClient sc : m_client_list)
		{
			System.out.println("Info: Starting handler thread for " + sc.name);
			new MazewarServerClientHandler(this, sc).start();
		}
		System.out.println("Info: Starting the bullet ticket thread");
		new MazewarServerBulletCounter(this).start();
		System.out.println("Info: Server is now in broadcast mode...");
		while(m_keep_broadcasting)
		{
			MessagePacket m = dequeue_packet();
			try
			{
				broadcast(m);
			}
			catch(IOException e)
			{
				System.out.println("Warning: Server dropped message packet");
			}
		}
	}
	
	public static void main(String[] args) {
		int port = -1;
		boolean error = false;
		System.out.println("Info: ****************************** Mazewar Server ******************************");
		System.out.println("Info: Usage: java MazewarServer <port>");
		try
		{
			port = Integer.parseInt(args[0]);
		}
		catch(NumberFormatException e)
		{
			System.out.println("Error: Failed to parse port number from command line arguments. " +
					args[0] + " is not a valid port number.\n");
			error = true;
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Error: Insufficient command line arguments provided. Check usage info message above.");
			error = true;
		}
		if(error)
		{
			System.out.println("Warning: Could not load server port from command line arguments. "+
					"Default value will be used instead");
			port = 6666;
		}
		MazewarServer server = new MazewarServer();
		server.run(port);
	}
	
	// Lists
	ArrayList<ServerClient> m_client_list;
	ArrayList<MessagePacket> m_packet_queue;
	
	// Locks
	private final ReentrantLock m_client_list_lock = new ReentrantLock();
	private final ReentrantLock m_packet_queue_lock = new ReentrantLock();
	final Condition m_packet_queue_not_full;
	
	private boolean m_keep_broadcasting = true;
}
