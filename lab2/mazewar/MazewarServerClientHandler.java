import java.io.IOException;


public class MazewarServerClientHandler extends Thread {

	public MazewarServerClientHandler(MazewarServer server, ServerClient sc)
	{
		m_server = server;
		m_sc = sc;
	}
	
	private MessagePacket convert_client_to_server_packet(MessagePacket m) throws IOException
	{
		MessagePacket sm = new MessagePacket(m);
		switch(m.type)
		{
		case MessagePacket.CLIENT_UP: sm.type = MessagePacket.SERVER_UP; break;
		case MessagePacket.CLIENT_DOWN: sm.type = MessagePacket.SERVER_DOWN; break;
		case MessagePacket.CLIENT_LEFT: sm.type = MessagePacket.SERVER_LEFT; break;
		case MessagePacket.CLIENT_RIGHT: sm.type = MessagePacket.SERVER_RIGHT; break;
		case MessagePacket.CLIENT_SPACE: sm.type = MessagePacket.SERVER_SPACE; break;
		case MessagePacket.CLIENT_SPAWN: sm.type = MessagePacket.SERVER_SPAWN; break;
		case MessagePacket.CLIENT_EXIT: sm.type = MessagePacket.SERVER_EXIT; break;
		default:
			System.err.println("Error: Unknown packet type '" + m.type + "' from '" + m.client_name + "'");
			throw new IOException("Unknwon packet type from client");
		}
		return sm;
	}
	
	public void run()
	{
		System.out.println("Info: Client '" + m_sc.name + "' handler thread is alive");
		// Give the go ahead to the client
		MessagePacket m = new MessagePacket();
		m.type = MessagePacket.SERVER_SPAWN;
		m.client_name = m_sc.name;
		m.point = m_sc.initial_point;
		m.dir = m_sc.initial_direction;
		m_server.enqueue_packet(m);
		
		System.out.println("Info: Client " + m_sc.name + " is locked and loaded!");
		
		boolean keep_looping = true;
		while(keep_looping)
		{
			try
			{
				while((m = (MessagePacket)m_sc.clientinstream.readObject()) != null)
				{
					MessagePacket sm = convert_client_to_server_packet(m);
					if(m.type == MessagePacket.CLIENT_EXIT)
					{
						keep_looping = false;
						m_server.remove_client(m_sc);
						m_server.enqueue_packet(sm);
						// Enqueue packet above will not broadcast to
						// this client because it has been removed
						// from the list
						m_sc.clientoutstream.writeObject(sm);
						break;
					}
					m_server.enqueue_packet(sm);
				}
				keep_looping = false;
			}
			catch(IOException e)
			{
				System.out.println("Warning: Dropped packet from '" + m_sc.name + "' (IOException)");
			}
			catch(ClassNotFoundException e)
			{
				System.out.println("Warning: Dropped packet from '" + m_sc.name + "' (ClassNotFoundException");
			}
		}
		
		try
		{
			sleep(5000);
			m_sc.clientsock.close();
		}
		catch(IOException e)
		{
			
		}
		catch(InterruptedException e)
		{
		}
		System.out.println("Info: Client " + m_sc.name + " has gone down!");
	}
	
	private MazewarServer m_server;
	private ServerClient m_sc;

}
