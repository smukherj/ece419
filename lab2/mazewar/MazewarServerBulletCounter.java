
public class MazewarServerBulletCounter extends Thread {
	public MazewarServerBulletCounter(MazewarServer server)
	{
		m_server = server;
	}
	
	public void run()
	{
		System.out.println("Info: Bullet counter thread is running.");
		while(m_server.keep_broadcasting())
		{
			try
			{
				Thread.sleep(200);
			}
			catch(InterruptedException e)
			{
				
			}
			MessagePacket m = new MessagePacket();
			m.type = MessagePacket.SERVER_TICK;
			m_server.enqueue_packet(m);
		}
		
		// One last packet for the server so that
		// it can exit. This is for the case where
		// we got the stop broadcasting message before
		// the server
		MessagePacket m = new MessagePacket();
		m.type = MessagePacket.SERVER_TICK;
		m_server.enqueue_packet(m);
	}
	
	private MazewarServer m_server;
}
