import java.lang.Thread;
import java.io.*;

public class MazeImplEnqueueThread extends Thread {

	private MazeImpl maze = null;

	public MazeImplEnqueueThread(MazeImpl maze) {
		super("MazeImplEnqueueThread");
		assert(maze != null);
		this.maze = maze;
	}

	public void run() {
		Long packets_from_server = new Long(0);
		Long errors = new Long(0);
		while(!isInterrupted()) {
			/* Get a packet and put in in our queue */
 			MessagePacket packetFromServer = null;
			
			try {
 				packetFromServer = maze.getFromServer();
 				packets_from_server++;
			} catch (IOException e) {
				errors++;
				if(packets_from_server > 0 && errors > 5)
					System.out.println("Warning: MazeImplEnqueueThread: Failed to get packet from server.");
				if(packets_from_server == 0 && errors > 5)
				{
					// Server rejected our username
					MessagePacket m = new MessagePacket();
					m.client_name = this.maze.getLocalClient().getName();
					m.type = m.SERVER_EXIT;
					m.error_code = m.ERROR_DUPL_USER;
					maze.enqueue(m);
					break;
				}
			} catch (ClassNotFoundException e) {
				errors++;
				System.out.println("Warning: MazeImplEnqueueThread: ClassNotFoundException.");
			}
			
			if(packetFromServer != null) {
				maze.enqueue(packetFromServer);
			}
		}
	}
}
