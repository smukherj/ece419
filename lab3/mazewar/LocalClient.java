/*
Copyright (C) 2004 Geoffrey Alan Washburn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
 */

import java.io.*;
import java.net.*;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.concurrent.locks.ReentrantLock;

public class LocalClient extends Client implements KeyListener {

	private boolean m_ignore_keypress;
	private boolean m_deactivate;
	private ReentrantLock m_keypress_lock;
	private DirectedPoint m_last_known_location;
	/**
	 * Create a LocalClient. 
	 */
	public LocalClient(String name) {
		super(name);
		assert(name != null);
		m_ignore_keypress = true;
		m_deactivate = false;
		m_keypress_lock = new ReentrantLock();
		m_last_known_location = null;
	}
	
	public void set_last_known_location(DirectedPoint dp)
	{
		m_last_known_location = dp;
	}
	
	private DirectedPoint get_last_known_location()
	{
		DirectedPoint dp = null;
		if(m_last_known_location != null)
		{
			dp = m_last_known_location;
			m_last_known_location = null;
		}
		return dp;
	}
	
	public void permanently_deactivate()
	{
		m_keypress_lock.lock();
		m_deactivate = true;
		m_ignore_keypress = true;
		m_keypress_lock.unlock();
		System.out.println("Info: Local client permanently deactivated");
	}
	
	public void ignore_keypress()
	{
		m_keypress_lock.lock();
		m_ignore_keypress = true;
		m_keypress_lock.unlock();
		System.out.println("Info: Ignoring keypresses");
	}
	
	public void accept_keypress()
	{
		if(m_deactivate)
		{
			System.out.println("Info: Rejecting accept keypress request");
			return;
		}
		m_keypress_lock.lock();
		m_ignore_keypress = false;
		m_keypress_lock.unlock();
		System.out.println("Info: Accepting keypresses");
	}

	/* Register LocalClient with Mazewar server */
	public synchronized void spawn(){
		
		if(m_ignore_keypress)
		{
			return;
		}
		
		MessagePacket spawnPacket = new MessagePacket();

		spawnPacket.type = MessagePacket.CLIENT_SPAWN;
		spawnPacket.client_name = getName();
		DirectedPoint dp = get_last_known_location();
		if(dp != null)
		{
			spawnPacket.point = dp;
			spawnPacket.dir = dp.getDirection();
		}
		else
		{
			spawnPacket.point = maze.getSpawnPoint();
			spawnPacket.dir = maze.getSpawnDirection(spawnPacket.point);
		}
		spawnPacket.score = maze.get_local_score();

		maze.multicastEvent(spawnPacket, true);
	}

	/**
	 * Handle a key press.
	 * @param e The {@link KeyEvent} that occurred.
	 */
	public synchronized void keyPressed(KeyEvent e) {
		if(m_ignore_keypress)
		{
			return;
		}
		
		MessagePacket packetToServer = new MessagePacket();

		packetToServer.client_name = getName();

		// If the user pressed Q, invoke the cleanup code and quit. 
		if((e.getKeyChar() == 'q') || (e.getKeyChar() == 'Q')) {
			maze.acquire_coordination_lock();
			maze.stop_ticker_thread();
			permanently_deactivate();
			maze.getRobotClient().permanently_deactivate();
			maze.getRobotClient().wait_until_exit();
			packetToServer.type = MessagePacket.CLIENT_EXIT;
			maze.multicastEvent(packetToServer, true);
			// Up-arrow moves forward.
		} else if(e.getKeyCode() == KeyEvent.VK_UP) {
			packetToServer.type = MessagePacket.CLIENT_UP;
			maze.multicastEvent(packetToServer, true);
			// Down-arrow moves backward.
		} else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			packetToServer.type = MessagePacket.CLIENT_DOWN;
			maze.multicastEvent(packetToServer, true);
			// Left-arrow turns left.
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			packetToServer.type = MessagePacket.CLIENT_LEFT;
			maze.multicastEvent(packetToServer, true);
			// Right-arrow turns right.
		} else if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			packetToServer.type = MessagePacket.CLIENT_RIGHT;
			maze.multicastEvent(packetToServer, true);
			// Spacebar fires.
		} else if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			packetToServer.type = MessagePacket.CLIENT_SPACE;
			maze.multicastEvent(packetToServer, true);
		}
	}

	/**
	 * Handle a key release.
	 * @param e The {@link KeyEvent} that occurred.
	 */
	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Handle a key being typed.
	 * @param e The {@link KeyEvent} that occurred.
	 */
	public void keyTyped(KeyEvent e) {
	}

}
