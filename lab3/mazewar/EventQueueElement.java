import java.io.*;
import java.net.*;
import java.util.*;


public class EventQueueElement {
	private MessagePacket m_message;
	// The type of message. Sometimes an ack can arrive before the message
	// and we will have no clue about the type of the message to expect
	private int m_type;
	private int m_num_acks_left;
	
	// This is to keep track of the number of
	// acks we got before the actual message arrived
	private int m_unaccounted_acks;
	
	// HashSet of the LamportClock ids of clients
	// who have acked this element
	private HashSet<Long> m_clients_who_acked;
	
	public ObjectInputStream istream;
	public ObjectOutputStream ostream;
	public Socket socket;
	
	public EventQueueElement(int type)
	{
		m_type = type;
		m_message = null;
		m_num_acks_left = -1;
		m_unaccounted_acks = 0;
		istream = null;
		ostream = null;
		socket = null;
		m_clients_who_acked = new HashSet<Long>();
	}
	
	public synchronized void ack(Long client_id)
	{
		// This client has already acked. This can happen during
		// quitting
		if(m_clients_who_acked.contains(client_id))
		{
			return;
		}
		
		if(m_message == null)
		{
			m_unaccounted_acks++;
		}
		else
		{
			assert(m_unaccounted_acks == 0);
			m_num_acks_left--;
		}
		if((m_message != null) && (m_num_acks_left < 0))
		{
			new UnknownError("Negative Num Acks! " + m_message.typestr() + " from " +
		 m_message.id
		 ).printStackTrace();
		}
		
		m_clients_who_acked.add(client_id);
	}
	
	public synchronized void setMessage(MessagePacket m, int num_acks)
	{
		assert(m_message == null);
		m_message = m;
		if(m.type == MessagePacket.CLIENT_REG_DONE)
		{
			m_num_acks_left = m.num_packets;
		}
		else
		{
			m_num_acks_left = num_acks;
		}
		
		m_num_acks_left -= m_unaccounted_acks;
		m_unaccounted_acks = 0;
		
		if(m_num_acks_left < 0)
		{
			new UnknownError("Negative Num Acks! " + m_message.typestr()).printStackTrace();
		}
	}
	
	public synchronized boolean isAckConsistent()
	{
		// The number of acks left will be -1 if the
		// ack arrived before the event
		return (m_message == null) || (m_num_acks_left >= 0);
	}
	
	// Is this event ready for delivery?
	public synchronized boolean ready()
	{
		return (m_message != null) && (m_num_acks_left <= 0);
	}
	
	public synchronized MessagePacket message()
	{
		return m_message;
	}
	
	public synchronized int type()
	{
		return m_type;
	}
	
	public synchronized int num_acks()
	{
		return m_num_acks_left;
	}
	
}
