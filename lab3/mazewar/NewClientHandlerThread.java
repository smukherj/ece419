import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.ArrayList;

public class NewClientHandlerThread extends Thread {
	NewClientHandlerThread(Maze maze, 
			Socket clientsock,
			ObjectInputStream istream,
			ObjectOutputStream ostream)
	{
		m_maze = maze;
		m_clientsock = clientsock;
		m_istream = istream;
		m_ostream = ostream;
	}
	
	private void run_hello_protocol(MessagePacket m) throws IOException, ClassNotFoundException
	{
		ArrayList<ClientInfo> client_list = m_maze.get_client_list();
		m.type = MessagePacket.CLIENT_WASSUPBRO;
		m.num_packets = client_list.size();
		m.id = m_maze.getLClock().id();
		m.lclock = m_maze.getLClock();
		System.out.println("Info: Sending WASSUPBRO");
		synchronized(m_ostream)
		{
			m_ostream.writeObject(m);
		}
		
		for(int i = 0; i < client_list.size(); ++i)
		{
			ClientInfo c = client_list.get(i);
			m = new MessagePacket();
			m.type = MessagePacket.CLIENT_BROINFO;
			m.id = c.id;
			m.hostname = c.hostname;
			m.port = c.port;
			System.out.println("Info: Sending BROINFO");
			synchronized(m_ostream)
			{
				m_ostream.writeObject(m);
			}
		}
		System.out.println("Info: Hello Protocol finished");
	}
	
	private void handle_client_packet(MessagePacket m)
	{
		if(m.type == MessagePacket.CLIENT_REG
			|| m.type == MessagePacket.CLIENT_REG_DONE
			|| m.type == MessagePacket.CLIENT_EXIT
			|| m.type == MessagePacket.CLIENT_EXIT_DONE
			|| m.ack_type == MessagePacket.CLIENT_REG
			|| m.ack_type == MessagePacket.CLIENT_REG_DONE
			|| m.ack_type == MessagePacket.CLIENT_EXIT
			|| m.ack_type == MessagePacket.CLIENT_EXIT_DONE
			|| ArgumentParser.opts.debug())
		{
			System.out.printf("Info: (%d, %d) : %s from %d (%s)\n",
					m.lclock.id(),
					m.lclock.clock(),
					m.typestr(),
					m.id,
					m.client_name);
		}
		
		if(m.type == MessagePacket.CLIENT_ACK)
		{
			m_maze.handle_ack(m);
		}
		else if(m.type == MessagePacket.CLIENT_HI)
		{
			System.out.println("Info: Client " + m.id + " says hi");
		}
		else
		{
			m_maze.getLClock().receive(m.lclock.clock());
			if(m.type == MessagePacket.CLIENT_EXIT)
			{
				m_maze.getLocalClient().ignore_keypress();
				m_maze.stop_ticker_thread();
				m_maze.getRobotClient().pause();
			}
			m_maze.enqueueEvent(m, m_istream, m_ostream, m_clientsock);
		}
		if(m.type == MessagePacket.CLIENT_REG
				|| m.type == MessagePacket.CLIENT_REG_DONE
				|| m.type == MessagePacket.CLIENT_EXIT
				|| m.type == MessagePacket.CLIENT_EXIT_DONE
				|| m.ack_type == MessagePacket.CLIENT_REG
				|| m.ack_type == MessagePacket.CLIENT_REG_DONE
				|| m.ack_type == MessagePacket.CLIENT_EXIT
				|| m.ack_type == MessagePacket.CLIENT_EXIT_DONE
				|| ArgumentParser.opts.debug())
			{
				System.out.printf("Info: Finished processing (%d, %d) : %s from %d\n",
						m.lclock.id(),
						m.lclock.clock(),
						m.typestr(),
						m.id);
			}
	}
	
	public void run()
	{
		System.out.println("Info: Received a new connection from " + 
				m_clientsock.getInetAddress().getHostName() + ":" +
				m_clientsock.getPort());
		m_maze.stop_ticker_thread();
		if(m_maze.getLocalClient() != null)
		{
			m_maze.getLocalClient().ignore_keypress();
		}
		if(m_maze.getRobotClient() != null)
		{
			m_maze.getRobotClient().pause();
		}
		MessagePacket m;
		try
		{
			while(!interrupted() && ((m = (MessagePacket) m_istream.readObject()) != null))
			{
				if(m.type == MessagePacket.CLIENT_HELLO)
				{
					run_hello_protocol(m);
				}
				else
				{
					handle_client_packet(m);
				}
				
				// If this message packet was an ACK for an EXIT_DONE packet then
				// we need to exit if either we are exiting or the remote client
				// this thread is talking to is exiting.
				boolean is_client_exit_done_ack = (m.type == MessagePacket.CLIENT_ACK) 
				&& (m.ack_type == MessagePacket.CLIENT_EXIT_DONE);
				boolean am_I_exiting = (m_maze.getLClock().id() == m.lclock.id());
				boolean is_remote_exiting = (m.id == m.lclock.id());
				if(is_client_exit_done_ack && (am_I_exiting || is_remote_exiting))
				{
					break;
				}
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		System.out.println("Info: Terminating connection with " + 
				m_clientsock.getInetAddress().getHostName() + ":" +
				m_clientsock.getPort());
		
	}
	
	Maze m_maze;
	Socket m_clientsock;
	
	ObjectOutputStream m_ostream;
	ObjectInputStream m_istream;
}
