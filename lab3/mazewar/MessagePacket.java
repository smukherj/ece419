import java.io.Serializable;
 /**
 * MessagePacket
 * ============
 * 
 * Packet format of the packets exchanged between the Client(s) and the Server
 * 
 */

public class MessagePacket implements Serializable {

	/* client -> server */
	public static final int CLIENT_SPAWN	= 100; /* spawn */
	public static final int CLIENT_UP	= 101; /* move forward */
	public static final int CLIENT_DOWN	= 102; /* move backward */
	public static final int CLIENT_RIGHT	= 103; /* rotate clockwise */
	public static final int CLIENT_LEFT	= 104; /* rotate counter-clockwise */
	public static final int CLIENT_SPACE 	= 105; /* fire a bullet */
	public static final int CLIENT_EXIT     = 106; /* exit/quit the game */
	public static final int CLIENT_EXIT_DONE = 107;
	
	/**
	 * This is the packet sent on first contact by a client. The receiving
	 * client will send connection details of every other client it is
	 * communicating with to this client
	 */
	public static final int CLIENT_HELLO    = 108;
	/**
	 * This is the packet sent by the client in reply to the hello packet
	 * above. This message will also contain the number of CLIENT_BROINFO
	 * packets that will subsequently be sent
	 */
	public static final int CLIENT_WASSUPBRO   = 109;
	/**
	 * These packets are sent after the CLIENT_WASSUPBRO packets giving
	 * information regarding Bros connected to the sender client
	 */
	public static final int CLIENT_BROINFO = 110;
	
	/**
	 * This is the packet send by a client after it has done the HELLO thing
	 * with a client. It now tries to register in the game.
	 */
	public static final int CLIENT_REG      = 111;
	
	/**
	 * This is the packet send by a client after registration is complete.
	 * This is to indicate registration is complete.
	 */
	public static final int CLIENT_REG_DONE      = 112;
	
	/**
	 * Generic ACK for any message. The LamportClock uniquely identifies the
	 * message for which this is an ACK
	 */
	public static final int CLIENT_ACK = 113;

	/* bullet tick */
	public static final int CLIENT_TICK = 114;
	
	/* Coordination lock acquire/release/ack */
	public static final int COORD_LOCK_ACQUIRE = 115;
	public static final int COORD_LOCK_REL = 116;
	public static final int COORD_LOCK_ACK = 117;
	
	public static final int CLIENT_HI = 118;
	
	/* error codes */
	public static final int ERROR_UNKNOWN	= -101; /* unknown error */
	public static final int ERROR_DUPL_USER = -102; /* username already taken */
	
	public int type;
	/**
	 * This field is populated only for ACKs saying what 'type' of
	 * message above they are ACKing to
	 */
	public int ack_type;
	public String client_name;
	public Point point;
	public Direction dir;
	
	public int error_code;
	public int score; /* Contains score of client in a CLIENT_SPAWN packet */

	/**
	 * Specified in the CLIENT_WASSUPBRO packet saying
	 * how many subsequent 
	 */
	public int num_packets;
	public String hostname;
	public int port;
	public long id;
	public LamportClock lclock;
	
	public MessagePacket()
	{
		type = -1;
		ack_type = -1;
		client_name = null;
		point = null;
		dir = null;
		error_code = -1;
		score = -1;
		num_packets = -1;
		hostname = null;
		port = -1;
		id = -1;
		lclock = null;
	}
	/*
	 * Copy constructor
	 */
	public MessagePacket(MessagePacket m)
	{
		type = m.type;
		ack_type = m.ack_type;
		client_name = new String(m.client_name);
		if(m.point == null)
		{
			point = null;
			dir = null;
		}
		else
		{
			point = new Point(m.point);
			dir = new Direction(m.dir);
		}
		error_code = m.error_code;
		num_packets = m.num_packets;
		hostname = new String(m.hostname);
		port = m.port;
		id = new Long(m.id);
		lclock = new LamportClock(m.lclock.id(), m.lclock);
	}
	
	public String typestr()
	{
		switch(type)
		{
		case MessagePacket.CLIENT_REG: return "REG";
		case MessagePacket.CLIENT_REG_DONE: return "REG_DONE";
		case MessagePacket.CLIENT_SPAWN: return "SPAWN";
		case MessagePacket.CLIENT_ACK: return "ACK";
		case MessagePacket.CLIENT_WASSUPBRO: return "WASSUPBRO";
		case MessagePacket.CLIENT_BROINFO: return "BROINFO";
		case MessagePacket.CLIENT_HELLO: return "HELLO";
		case MessagePacket.CLIENT_UP: return "UP";
		case MessagePacket.CLIENT_DOWN: return "DOWN";
		case MessagePacket.CLIENT_LEFT: return "LEFT";
		case MessagePacket.CLIENT_RIGHT: return "RIGHT";
		case MessagePacket.CLIENT_SPACE: return "SPACE";
		case MessagePacket.CLIENT_TICK: return "TICK";
		case MessagePacket.CLIENT_EXIT: return "EXIT";
		case MessagePacket.CLIENT_EXIT_DONE: return "EXIT_DONE";
		case MessagePacket.COORD_LOCK_ACQUIRE: return "LOCK_ACQUIRE";
		case MessagePacket.COORD_LOCK_REL: return "LOCK_REL";
		case MessagePacket.COORD_LOCK_ACK: return "LOCK_ACK";
		case MessagePacket.CLIENT_HI: return "HI";
		default: return Integer.toString(type);
		}
	}
}
