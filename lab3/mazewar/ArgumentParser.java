import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgumentParser {

	public ArgumentParser(String[] args)
	{
		Pattern argvalpattern = Pattern.compile("--?([^=]+)=(.+)");
		Pattern argpattern = Pattern.compile("--?(.+)");
		for(String arg : args)
		{
			Matcher m = argvalpattern.matcher(arg);
			if(m.find())
			{
				process_arg(m.group(1), m.group(2));
				continue;
			}
			m = argpattern.matcher(arg);
			if(m.find())
			{
				process_arg(m.group(1), null);
				continue;
			}
			System.out.println("Error: Failed to parse argument '" + arg + "'");
			m_errors = true;
		}
		if(args.length == 0)
		{
			display_help();
		}
		
		if(!exit())
		{
			check_args();
		}
		
		if(ArgumentParser.opts == null)
		{
			opts = this;
		}
		else
		{
			new Throwable("Error: Arguments being parsed multiple times!").printStackTrace();
			System.exit(-1);
		}
	}
	
	private void process_arg(String arg, String val)
	{
		//System.out.println("Processing arg: '" + arg + "', val: '" + (val != null ? val : "null") + "'");
		arg = arg.toLowerCase();
		
		// First parse arguments that don't require values
		if(arg.equals("help"))
		{
			display_help();
			m_exit = true;
			return;
		}
		else if(arg.equals("log"))
		{
			m_log = true;
			return;
		}
		else if(arg.equals("debug"))
		{
			m_log = true;
			m_debug = true;
			return;
		}
		
		// Now parse arguments that require values
		if(val == null)
		{
			System.out.println("Error: Argument '" + arg + "' is either unrecognized or missing a value");
			m_errors = true;
			return;
		}
		
		if(arg.equals("listenport"))
		{
			int port = -1;
			try
			{
				port = Integer.parseInt(val);
			}
			catch(NumberFormatException e)
			{
				System.out.println("Error: Value '" + val + "' for argument '" + arg + "' is invalid");
				m_errors = true;
			}
			m_listen = true;
			m_listenport = port;
		}
		else if(arg.equals("hostname"))
		{
			m_hostname = val;
		}
		else if(arg.equals("cshost"))
		{
			m_cshost = val;
		}
		else if(arg.equals("csport"))
		{
			int port = -1;
			try
			{
				port = Integer.parseInt(val);
			}
			catch(NumberFormatException e)
			{
				System.out.println("Error: Value '" + val + "' for argument '" + arg + "' is invalid");
				m_errors = true;
			}
			m_csport = port;
		}
		else if(arg.equals("port"))
		{
			int port = -1;
			try
			{
				port = Integer.parseInt(val);
			}
			catch(NumberFormatException e)
			{
				System.out.println("Error: Value '" + val + "' for argument '" + arg + "' is invalid");
				m_errors = true;
			}
			m_port = port;
		}
		else
		{
			System.out.println("Error: Unrecognized argument: '" + arg + "'");
			m_errors = true;
		}
	}
	
	private void check_args()
	{
		if((m_hostname != null) || (m_port > 0))
		{
			if((m_hostname == null) || (m_port == -1))
			{
				if(m_hostname == null)
				{
					System.out.println("Error: Port number for the remote client hosting the game was provided, ");
					System.out.println("Error: however the hostname was not");
				}
				else
				{
					System.out.println("Error: Hostname for the remote client hosting the game was provided, ");
					System.out.println("Error: however the port number was not");
				}
				m_errors = true;
			}
			else
			{
				m_connect = true;
			}
		}
		
		if(!m_listen)
		{
			System.out.println("Error: A port for the client to listen for connections was not provided");
			m_errors = true;
		}
		
		if(m_cshost == null || m_csport == -1)
		{
			System.out.println("Warning: Coordination server connection details were not provided.");
			System.out.println("         The game may encounter consistency issues during dynamic");
			System.out.println("         player join/quit.");
			m_cshost = null;
			m_csport = -1;
		}
	}
	
	private void display_help()
	{
		// We always exit after displaying help
		m_exit = true;
		System.out.println("Info: --help                            Display this message and exit");
		System.out.println("Info:");
		System.out.println("Info: Mandatory Arguments");
		System.out.println("Info: --listenport=<port number>        Listen for incoming connections from other clients");
		System.out.println("Info:                                   at the given port number.");
		System.out.println("Info:");
		System.out.println("Info: Optional Arguments");
		System.out.println("Info: --hostname=<hostname>             When this client comes online it will attempt to");
		System.out.println("Info:                                   connect to this hostname to register in the game.");
		System.out.println("Info:                                   This option is not required if this is the first");
		System.out.println("Info:                                   client in the game.");
		System.out.println("Info: --port=<port number>              When this client comes online it will attempt to");
		System.out.println("Info:                                   connect to this port to register in the game.");
		System.out.println("Info:                                   This option is not required if this is the first");
		System.out.println("Info:                                   client in the game.");
		System.out.println("Info: --cshost=<hostname>               Hostname of the coordination server");
		System.out.println("Info: --csport=<port number>            Port number of the coordination server");
		System.out.println("Info: --log                             Turn on logging of events for debugging.");
		System.out.println("Info: --debug                           Turn on extra verbose debug messages. Also turns");
		System.out.println("Info:                                   on logging");
		
	}
	
	public boolean exit()
	{
		return m_exit || m_errors;
	}
	
	public boolean errors()
	{
		return m_errors;
	}
	
	public boolean listen()
	{
		return m_listen;
	}
	
	public int listenport()
	{
		return m_listenport;
	}
	
	public String hostname()
	{
		return m_hostname;
	}
	
	public String cshost()
	{
		return m_cshost;
	}
	
	public int csport()
	{
		return m_csport;
	}
	
	public int port()
	{
		return m_port;
	}
	
	public boolean log()
	{
		return m_log;
	}
	
	public boolean debug()
	{
		return m_debug;
	}
	
	public boolean connect()
	{
		return m_connect;
	}
	
	// This cliet is a host if it has
	// no remote host to connect to and
	// it is listening for incoming connections
	// from other clients
	public boolean isHost()
	{
		return (!m_connect && m_listen);
	}
	
	private boolean m_listen = false; // Should this client listen for connections?
	private int m_listenport = -1; // What port should this client listen for connections on?
	private String m_cshost = null; // Hostname of the coordination server
	private int m_csport = -1;
	private String m_hostname = null; // Which hostname is hosting the game?
	private int m_port = -1; // What is the port of the host hosting the game?
	private boolean m_connect = false; // Should this client connect to someone hosting a game?
	private boolean m_log = false; // Option to turn on logging of event order
	private boolean m_debug = false; // Option to turn on extra verbose debug log messages
	
	private boolean m_exit = false; // Should the client exit right after argument processing?
	private boolean m_errors = false; // Were there any errors in argument processing?
	
	public static ArgumentParser opts = null;
}
