import java.io.*;
import java.lang.Thread;

public class MazeImplDequeueThread extends Thread {

	private MazeImpl maze = null;
	private FileWriter logfile = null;
	private int logerror = 0;

	public MazeImplDequeueThread(MazeImpl maze, boolean log) {
		super("MazeImplDequeueThread");
		assert(maze != null);
		this.maze = maze;
		
		if(log)
		{
			String filename = new String("events_" + maze.getLClock().id() + ".log");
			try
			{
				logfile = new FileWriter(filename);
				System.out.println("Info: Events will be logged to '" + filename + "'");
			}
			catch(IOException e)
			{
				System.out.println("Warning: Failed to open log file for writing event order");
			}
		}
	}
	
	private void log(MessagePacket m)
	{
		if(logfile == null)
		{
			return;
		}
		try
		{
			String logstr = new String("Info: Event: Clock: " + m.lclock.toString() + ", type: " + m.typestr() +
			", client: " + m.client_name + "\n");
			System.out.print(logstr);
			logfile.write(logstr);
			logfile.flush();
		}
		catch(IOException e)
		{
			logerror++;
			if(logerror < 5)
			{
				System.out.println("Warning: Failed to write to log " + logerror + " times");
			}
			else
			{
				System.out.println("Warning: Failed to write to log " + logerror + " times. Turning off logging");
				logfile = null;
			}
		}
	}

	public void run() {
		
		while(!isInterrupted()) {
			Client client;

			EventQueueElement e = maze.dequeue();
			MessagePacket nextPacket = e.message();
			assert(nextPacket != null);
			log(nextPacket);
			
			switch(nextPacket.type){
			case MessagePacket.ERROR_UNKNOWN:
				System.out.println("Warning: MazeImplDequeueThread: Error packet receieved from server.");
				break;
			case MessagePacket.CLIENT_REG:
				System.out.println("Info: Adding new client with id: " + nextPacket.lclock.id());
				maze.handle_new_client_registration(e);
				break;
			case MessagePacket.CLIENT_REG_DONE:
				if(nextPacket.id == maze.getLClock().id())
				{
					maze.release_coordination_lock();
				}					
				maze.getLocalClient().accept_keypress();
				maze.getLocalClient().spawn();
				maze.getRobotClient().spawn();
				maze.getRobotClient().activate();
				maze.start_ticker_thread();
				break;
			case MessagePacket.CLIENT_SPAWN:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					// When the local client is being spawned for the first time,
					// it wouldn't have been registered in the client map
					LocalClient local_client = maze.getLocalClient();
					RobotClient bot_client = maze.getRobotClient();
					if((local_client != null) && (nextPacket.client_name.equals(local_client.getName())))
					{
						client = maze.getLocalClient();
					}
					else if ((bot_client != null) && (nextPacket.client_name.equals(bot_client.getName()))) {
						client = maze.getRobotClient();
					}
					else
					{
						client = new RemoteClient(nextPacket.client_name);
						maze.addClient(client);
					}
				}
				maze.spawnClient(client, nextPacket.point, nextPacket.dir);
				maze.set_client_score(client, nextPacket.score);
				break;
			case MessagePacket.CLIENT_UP:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.forward();
				}
				break;
			case MessagePacket.CLIENT_DOWN:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.backup();
				}
				break;
			case MessagePacket.CLIENT_RIGHT:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.turnRight();
				}
				break;
			case MessagePacket.CLIENT_LEFT:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.turnLeft();
				}
				break;
			case MessagePacket.CLIENT_SPACE:
				client = maze.getClient(nextPacket.client_name);
				if(client == null){
					System.out.println("Warning: MazeImplDequeueThread: client: " + nextPacket.client_name + " not found in maze.");
				} else{
					client.fire();
				}
				break;
			case MessagePacket.CLIENT_EXIT:
				if(e.message().id == maze.getLClock().id())
				{
					System.out.println("Info: Initiating quit protocol");
					MessagePacket m = new MessagePacket();
					m.id = maze.getLClock().id();
					m.type = MessagePacket.CLIENT_EXIT_DONE;
					m.client_name = maze.getLocalClient().getName();
					maze.multicastEvent(m, true);
				}
				else
				{
					System.out.println("Info: " + e.message().client_name + " has initiated quit protocol!");
				}
				break;
			case MessagePacket.CLIENT_EXIT_DONE:
				System.out.println("Info: Finalizing quit for " + e.message().client_name);
				maze.handle_client_quitting(e);
				System.out.println("Info: Client connection information successfully removed");
				maze.start_ticker_thread();
				maze.getLocalClient().accept_keypress();
				maze.getRobotClient().activate();
				break;
			case MessagePacket.CLIENT_TICK:
				maze.handleProjectiles();
				break;
			default:
				System.out.println("Error: MazeImplDequeueThread: MessagePacket type '" + nextPacket.typestr() + "' not supported.");
				//System.exit(-1);
				break;
			}
		}
	}
}
