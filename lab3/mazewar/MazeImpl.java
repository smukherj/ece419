/*
Copyright (C) 2004 Geoffrey Alan Washburn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
 */
/*
import java.lang.Thread;
import java.lang.Runnable;
import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;  
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;
 */

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

/**
 * A concrete implementation of a {@link Maze}.  
 * @author Geoffrey Washburn &lt;<a href="mailto:geoffw@cis.upenn.edu">geoffw@cis.upenn.edu</a>&gt;
 * @version $Id: MazeImpl.java 371 2004-02-10 21:55:32Z geoffw $
 */

public class MazeImpl extends Maze implements Serializable, ClientListener {

	/**
	 * Create a {@link Maze}.
	 * @param point Treat the {@link Point} as a magintude specifying the
	 * size of the maze.
	 * @param seed Initial seed for the random number generator.
	 */
	public MazeImpl(Point point, long seed, ArgumentParser opts,
		ScoreTableModel score_table) {
		m_opts = opts;
		m_score_table = score_table;
		maxX = point.getX();
		assert(maxX > 0);
		maxY = point.getY();
		assert(maxY > 0);

		// Initialize the maze matrix of cells
		mazeVector = new Vector(maxX);
		for(int i = 0; i < maxX; i++) {
			Vector colVector = new Vector(maxY);

			for(int j = 0; j < maxY; j++) {
				colVector.insertElementAt(new CellImpl(), j);
			}

			mazeVector.insertElementAt(colVector, i);
		}

		// Initialize the random number generator
		randomGen = new Random(seed);

		// Build the maze starting at the corner
		buildMaze(new Point(0,0));

		// Initialize our packet queue and its lock
		packetQueueLock = new ReentrantLock();
		packetQueueEmpty = packetQueueLock.newCondition();
		m_client_list = new ArrayList<ClientInfo>();
		m_client_list_lock = new ReentrantLock();
		m_lclock_queue = new PriorityQueue<LamportClock>(100, new LamportClockComparator());
		m_clock_to_event_map = new HashMap<LamportClock, EventQueueElement>();
		m_exit_protocol_initiated = false;
		
		// Attempt connection with coordination server
		connect_with_coordination_server();
		
		// Start the thread that listens for connections from
		// other clients
		m_listener_thread = new ListenerThread(this, m_opts);
		m_listener_thread.start();
		
		// Connect to a remote host hosting a Mazewar game if
		// connection details were provided.
		if(m_opts.connect())
		{
			try
			{
				do_game_registration();
			}
			catch(IOException e)
			{
				System.out.println("Failed to connect to the game hosted by " + opts.hostname() + ":" + opts.port());
				e.printStackTrace();
				System.exit(-1);
			}
			catch(ClassNotFoundException e)
			{
				System.out.println("Failed to connect to the game hosted by " + opts.hostname() + ":" + opts.port());
				e.printStackTrace();
				System.exit(-1);
			}
		}
		else // We're the first player/host in this game
		{
			m_lclock = new LamportClock(0);
			System.out.println("Info: Claiming id: 0");
			start_ticker_thread();
		}

		dequeueThread = new MazeImplDequeueThread(this, m_opts.log());
	}
	
	public void start_dequeue_thread()
	{
		dequeueThread.start();
	}

	public void start_ticker_thread()
	{
		if(bulletTickerThread != null || !am_I_lowest_client_id())
			return;
		
		bulletTickerLock = new ReentrantLock();
		bulletTickerThread = new BulletTickerThread(this, bulletTickerLock);
		bulletTickerThread.start();
	}
	
	public void stop_ticker_thread()
	{
		if(bulletTickerThread == null)
		{
			return;
		}
		bulletTickerThread.interrupt();
		bulletTickerLock.lock();
		bulletTickerThread = null;
		bulletTickerLock.unlock();
		bulletTickerLock = null;
		System.out.println("Info: Bullet ticker stopped");
	}
	
	boolean am_I_lowest_client_id()
	{
		boolean result = true;
		long my_id = m_lclock.id();
		m_client_list_lock.lock();
		try
		{
			for(ClientInfo c : m_client_list)
			{
				if(c.id < my_id)
				{
					result = false;
					break;
				}
			}
		}
		finally
		{
			m_client_list_lock.unlock();
		}
		return result;
	}
	
	long get_unused_id_from_client_list()
	{
		long max_id = -1;
		m_client_list_lock.lock();
		try
		{
			for(ClientInfo c : m_client_list)
			{
				if(c.id > max_id)
				{
					max_id = c.id;
				}
			}
		}
		finally
		{
			m_client_list_lock.unlock();
		}
		if(max_id == -1)
		{
			new UnknownError("No client in client list but trying to do game registration!").printStackTrace();
		}
		return max_id + 1;
	}

	public int get_local_score()
	{
		int score = 0;
		try
		{
			score = m_score_table.getClientScore(m_local_client);
		}
		finally
		{

		}
		return score;
	}
	
	public int get_bot_score()
	{
		int score = 0;
		try
		{
			score = m_score_table.getClientScore(m_bot_client);
		}
		finally
		{

		}
		return score;
	}

    public void set_client_score(Client client, int score)
    {
    	try
    	{
    		m_score_table.setClientScore(client, score);
    	}
    	finally
    	{

    	}
    }
	
	private void connect_with_coordination_server()
	{
		m_coordination_server_socket = null;
		m_coordination_server_ostream = null;
		m_coordination_server_istream = null;
		m_have_coordination_lock = false;
		m_local_coordination_lock = new ReentrantLock();
		boolean error = true;
		
		try
		{
			
			m_coordination_server_socket = new Socket(m_opts.cshost(), m_opts.csport());
			m_coordination_server_ostream = new ObjectOutputStream(m_coordination_server_socket.getOutputStream());
			m_coordination_server_istream = new ObjectInputStream(m_coordination_server_socket.getInputStream());
			error = false;
		}
		catch(IOException e)
		{
			
		}
		catch(IllegalArgumentException e)
		{
			
		}
		
		if(error)
		{
			System.out.println("Warning: Dynamic join/quit coordination could not be enabled.");
			System.out.println("         This may be because the server could not be contacted");
			System.out.println("          or connection details were not provided.");
		}
	}
	
	public String get_client_name()
	{
		if(m_local_client != null)
		{
			return m_local_client.getName();
		}
		else
		{
			return "<unknown>";
		}
	}
	
	public void acquire_coordination_lock()
	{
		System.out.println("Info: Attempting to acquire coordination lock");
		if(m_have_coordination_lock)
		{
			new Throwable("Fatal Error: Attempt to acquire coordination lock when it is already held!").printStackTrace();
			System.exit(-1);
		}
		m_local_coordination_lock.lock();
		try
		{
			if(m_coordination_server_socket == null)
			{
				m_have_coordination_lock = true;
				System.out.println("Info: Coordination lock acquired");
				return;
			}

			try
			{
				MessagePacket m = new MessagePacket();
				m.type = MessagePacket.COORD_LOCK_ACQUIRE;
				m.client_name = get_client_name();
				synchronized(m_coordination_server_ostream)
				{
					m_coordination_server_ostream.writeObject(m);
				}
				m = (MessagePacket)m_coordination_server_istream.readObject();
				if(m.type != MessagePacket.COORD_LOCK_ACK)
				{
					new Throwable("Fatal Error: Received a message of type '" 
							+ m.typestr() + "' from the coordination server").printStackTrace();
					System.exit(-1);
				}
				m_have_coordination_lock = true;
			}
			catch(IOException e)
			{

			}
			catch(ClassNotFoundException e)
			{

			}

			if(!m_have_coordination_lock)
			{
				new Throwable("Fatal Error: Failed to acquire coordination lock").printStackTrace();
				System.exit(-1);
			}
			System.out.println("Info: Coordination lock acquired");
		}
		finally
		{
			m_local_coordination_lock.unlock();
		}
	}
	
	public void release_coordination_lock()
	{
		System.out.println("Info: Attempting to release coordination lock");
		if(m_have_coordination_lock == false)
		{
			new Throwable("Fatal Error: Attempt to release coordination lock when it is not held!").printStackTrace();
			System.exit(-1);
		}
		m_local_coordination_lock.lock();
		try
		{
			if(m_coordination_server_socket == null)
			{
				m_have_coordination_lock = false;
				System.out.println("Info: Coordination lock released");
				return;
			}

			try
			{
				MessagePacket m = new MessagePacket();
				m.type = MessagePacket.COORD_LOCK_REL;
				m.client_name = get_client_name();
				synchronized(m_coordination_server_ostream)
				{
					m_coordination_server_ostream.writeObject(m);
				}
				m = (MessagePacket)m_coordination_server_istream.readObject();
				if(m.type != MessagePacket.COORD_LOCK_ACK)
				{
					new Throwable("Fatal Error: Received a message of type '" 
							+ m.typestr() + "' from the coordination server").printStackTrace();
					System.exit(-1);
				}
				m_have_coordination_lock = false;
			}
			catch(IOException e)
			{

			}
			catch(ClassNotFoundException e)
			{

			}
			if(m_have_coordination_lock)
			{
				new Throwable("Fatal Error: Failed to release coordination lock").printStackTrace();
				System.exit(-1);
			}
			System.out.println("Info: Coordination lock released");
		}
		finally
		{
			m_local_coordination_lock.unlock();
		}
	}
	
	public ArrayList<ClientInfo> get_client_list()
	{
		ArrayList<ClientInfo> client_list = new ArrayList<ClientInfo>();
		m_client_list_lock.lock();
		try
		{
			for(ClientInfo ic : m_client_list)
			{
				ClientInfo c = new ClientInfo(ic);
				client_list.add(c);
			}
		}
		finally
		{
			m_client_list_lock.unlock();
		}
		
		return client_list;
	}
	
	void do_game_registration() throws IOException, ClassNotFoundException
	{
		acquire_coordination_lock();
		
		// Wait a little bit to allow the clients in the game to settle down.
		// A client might have just finished registering or quit the game.
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException err)
		{
			
		}
		
		Socket socket = null;
		try
		{
			socket = new Socket(m_opts.hostname(), m_opts.port());
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to game hosted at " +
					m_opts.hostname() + ":" + m_opts.port());
			System.out.println("Error: Message from the socket '" + e.getMessage() +"'");
			System.exit(-1);
		}
		
		String host_addr = socket.getInetAddress().getHostAddress();
		ObjectOutputStream ostream = new ObjectOutputStream(socket.getOutputStream());
		ObjectInputStream istream = new ObjectInputStream(socket.getInputStream());
		ClientInfo c = new ClientInfo();
		c.hostname = host_addr;
		c.port = m_opts.port();
		c.ostream = ostream;
		c.istream = istream;
		c.socket = socket;
		System.out.println("Info: Established connection with client " + c.hostname +
				":" + c.port);
		
		Object o;
		MessagePacket m = new MessagePacket();
		m.type = MessagePacket.CLIENT_HELLO;
		synchronized(ostream)
		{
			ostream.writeObject(m);
		}
		o = istream.readObject();
		m = (MessagePacket)o;
		if(m.type != MessagePacket.CLIENT_WASSUPBRO)
		{
			System.out.println("Error: Protocol Error. Expected CLIENT_WASSUPBRO packet after HELLO packet from remote client");
			System.exit(-1);
		}
		
		int num_packets = m.num_packets;
		LamportClock remote_lclock = m.lclock;
		c.id = m.id;
		add_client(c);
		ArrayList<NewClientHandlerThread> client_handler_threads = new ArrayList<NewClientHandlerThread>();
		client_handler_threads.add(new NewClientHandlerThread(this, c.socket, c.istream, c.ostream));
		
		for(int i = 0; i < num_packets; ++i)
		{
			m = (MessagePacket)istream.readObject();
			if(m.type != MessagePacket.CLIENT_BROINFO)
			{
				System.out.println("Error: Protocol Error. Expected " + (num_packets - i) +
						" more CLIENT_BROINFO packets after CLIENT_WASSUPBRO packet from remote client");
				System.out.println("Error: Received " + (i + 1) + " packets. Message type was " + m.type);
				System.exit(-1);
			}
			c = new ClientInfo();
			c.id = m.id;
			c.hostname = m.hostname;

			try
			{
				InetAddress addr = InetAddress.getByName(c.hostname);
				// The client from whom we are getting connection details of other
				// clients has provided a "localhost" client. But we need to use the
				// actual hostname to connect to them which is the hostname as we see
				// of that client
				if(addr.isAnyLocalAddress() || addr.isLoopbackAddress())
				{
					System.out.println("Info: Resolving foreign localhost address " +
						c.hostname + " to " + m_opts.hostname() + " (" +
							host_addr + ")");
					c.hostname = host_addr;
				}
			}
			catch(UnknownHostException e)
			{
				System.out.println("Error: Could not resolve client hostname " + c.hostname);
				System.exit(-1);
			}

			c.port = m.port;
			try
			{
				c.socket = new Socket(c.hostname, c.port);
			}
			catch(IOException e)
			{
				System.out.println("Error: Failed to connect to client " +
					c.hostname + ":" + c.port);
				System.exit(-1);
			}
			System.out.println("Info: Established connection with client " + c.hostname +
				":" + c.port);
			c.ostream = new ObjectOutputStream(c.socket.getOutputStream());
			c.istream = new ObjectInputStream(c.socket.getInputStream());
			add_client(c);
			client_handler_threads.add(new NewClientHandlerThread(this, c.socket, c.istream, c.ostream));
		}
		
		m_lclock = new LamportClock(get_unused_id_from_client_list());
		m_lclock.receive(remote_lclock.clock());
		m_lclock.increment(5000);
		System.out.println("Info: Claiming id: " + m_lclock.id());
		
		m = new MessagePacket();
		m.type = MessagePacket.CLIENT_HI;
		m.lclock = new LamportClock(m_lclock);
		m.id = m_lclock.id();
		broadcastPacket(m);
		
		// We have made first contact with all clients. Again sleep for sometime
		// because the clients will stop generating messages and we want the messages
		// currently in flight to reach all clients before we start the registration
		// process. This will ensure all packets prior to our registration are
		// successfully flushed
		try
		{
			Thread.sleep(2000);
		}
		catch(InterruptedException err)
		{
			
		}
		
		m = new MessagePacket();
		m.type = MessagePacket.CLIENT_REG;
		m.lclock = new LamportClock(m_lclock);
		m.id = m_lclock.id();
		m.port = m_opts.listenport();
		// Don't broadcast an ACK from our side because the other clients haven't added us
		// yet and are not expecting an ACK
		multicastEvent(m, false);
		System.out.println("Info: Hello Protocol finished. Added " + get_client_list_size() + " clients");
		
		// Now start all the threads to handle incoming messages from the clients
		for(NewClientHandlerThread t : client_handler_threads)
		{
			t.start();
		}
	}
	
	private int get_client_list_size()
	{
		m_client_list_lock.lock();
		int size = m_client_list.size();
		m_client_list_lock.unlock();
		return size;
	}
	
	private void add_client(ClientInfo c)
	{
		m_client_list_lock.lock();
		try
		{
			for(ClientInfo ic : m_client_list)
			{
				if(ic.id == c.id)
				{
					System.out.println("Error: Adding a client with duplicate id: " + ic.id);
					new Throwable().printStackTrace();
					System.exit(-1);
				}
			}

			m_client_list.add(c);
		}
		finally
		{
			m_client_list_lock.unlock();
		}
	}
	
	private void remove_client(long client_id)
	{
		m_client_list_lock.lock();
		try
		{
			ClientInfo to_remove = null;
			for(ClientInfo ic : m_client_list)
			{
				if(ic.id == client_id)
				{
					to_remove = ic;
					break;
				}
			}
			if(to_remove == null)
			{
				System.out.println("Error: Client with id: " + client_id + " did not exist.");
				new Throwable().printStackTrace();
				System.exit(-1);
			}
			m_client_list.remove(to_remove);
		}
		finally
		{
			m_client_list_lock.unlock();
		}
	}

	/** 
	 * Create a maze from a serialized {@link MazeImpl} object written to a file.
	 * @param mazefile The filename to load the serialized object from.
	 * @return A reconstituted {@link MazeImpl}. 
	 */
	public static Maze readMazeFile(String mazefile)
			throws IOException, ClassNotFoundException {
		assert(mazefile != null);
		FileInputStream in = new FileInputStream(mazefile);
		ObjectInputStream s = new ObjectInputStream(in);
		Maze maze = (Maze) s.readObject();

		return maze;
	}

	/** 
	 * Serialize this {@link MazeImpl} to a file.
	 * @param mazefile The filename to write the serialized object to.
	 * */
	public void save(String mazefile)
			throws IOException {
		assert(mazefile != null);
		FileOutputStream out = new FileOutputStream(mazefile);
		ObjectOutputStream s = new ObjectOutputStream(out);
		s.writeObject(this);
		s.flush();
	}

	/** 
	 * Display an ASCII version of the maze to stdout for debugging purposes.  
	 */
	public void print() {
		for(int i = 0; i < maxY; i++) {
			for(int j = 0; j < maxX; j++) {
				CellImpl cell = getCellImpl(new Point(j,i));
				if(j == maxY - 1) {
					if(cell.isWall(Direction.South)) {
						System.out.print("+-+");
					} else {
						System.out.print("+ +");
					}
				} else {
					if(cell.isWall(Direction.South)) {
						System.out.print("+-");
					} else {
						System.out.print("+ ");
					}
				}

			}	    
			System.out.print("\n");
			for(int j = 0; j < maxX; j++) {
				CellImpl cell = getCellImpl(new Point(j,i));
				if(cell.getContents() != null) {
					if(cell.isWall(Direction.West)) {
						System.out.print("|*");
					} else {
						System.out.print(" *");
					}
				} else {
					if(cell.isWall(Direction.West)) {
						System.out.print("| ");
					} else {
						System.out.print("  ");
					}
				}
				if(j == maxY - 1) {
					if(cell.isWall(Direction.East)) {
						System.out.print("|");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.print("\n");
			if(i == maxX - 1) {
				for(int j = 0; j < maxX; j++) {
					CellImpl cell = getCellImpl(new Point(j,i));
					if(j == maxY - 1) {
						if(cell.isWall(Direction.North)) {
							System.out.print("+-+");
						} else {
							System.out.print("+ +");
						}
					} else {
						if(cell.isWall(Direction.North)) {
							System.out.print("+-");
						} else {
							System.out.print("+ ");
						}
					}		
				}
				System.out.print("\n");     
			}   
		}

	}


	public boolean checkBounds(Point point) {
		assert(point != null);
		return (point.getX() >= 0) && (point.getY() >= 0) && 
				(point.getX() < maxX) && (point.getY() < maxY);
	}

	public Point getSize() {
		return new Point(maxX, maxY);
	}

	public synchronized Cell getCell(Point point) {
		assert(point != null);
		return getCellImpl(point);
	}

	public void setLocalClient(LocalClient client)
	{
		assert(m_local_client == null);
		m_local_client = client;
	}

	public void setRobotClient(RobotClient client)
        {
                assert(m_bot_client == null);
                m_bot_client = client;
        }

	public LocalClient getLocalClient()
	{
		return m_local_client;
	}

	public RobotClient getRobotClient()
	{
		return m_bot_client;
	}

	public synchronized void spawnClient(Client client, Point point, Direction direction){
		assert(client != null);
		assert(point != null);
		assert(direction != null);
		// Make sure requested spawn cell is still empty
		CellImpl cell = getCellImpl(point);
		if(cell.getContents() != null){ // Not empty
			// If LocalClient, try again, else ignore.
			if(client instanceof LocalClient){
				((LocalClient)client).spawn();
			} else if (client instanceof RobotClient) {
				((RobotClient)client).spawn();
			}
			else /* RemoteClient */
			{
				// nullify the remote client's position
				clientMap.put(client, null);
			}
		} else{ // Still empty; put client on the map
			cell.setContents(client);
			clientMap.put(client, new DirectedPoint(point, direction));
			update();
			if(client instanceof RobotClient)
			{
				if(!getRobotClient().started())
					getRobotClient().start();
			}
		}
	}

	public synchronized void addClient(Client client) {
		client.registerMaze(this);
		client.addClientListener(this);
		update();
		notifyClientAdd(client);
		/*
		assert(client != null);
                // Pick a random starting point, and check to see if it is already occupied
                Point point = new Point(randomGen.nextInt(maxX),randomGen.nextInt(maxY));
                CellImpl cell = getCellImpl(point);
                // Repeat until we find an empty cell
                while(cell.getContents() != null) {
                        point = new Point(randomGen.nextInt(maxX),randomGen.nextInt(maxY));
                        cell = getCellImpl(point);
                } 
                addClient(client, point);
		 */
	}

	public synchronized Point getSpawnPoint(){
		// Pick a random starting point
		Point point = new Point(randomGen.nextInt(maxX),randomGen.nextInt(maxY));
		CellImpl cell = getCellImpl(point);
		// Repeat until we find an empty cell
		while(cell.getContents() != null) {
			point = new Point(randomGen.nextInt(maxX),randomGen.nextInt(maxY));
			cell = getCellImpl(point);
		}
		return point;
	}

	public synchronized Direction getSpawnDirection(Point point){
		assert(point != null);
		CellImpl cell = getCellImpl(point);
		// Pick a random starting direction
		Direction d = Direction.random();
		while(cell.isWall(d)) {
			d = Direction.random();
		}
		return d;
	}

	public synchronized Point getClientPoint(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		assert(o instanceof Point);
		return (Point)o;
	}

	public synchronized Direction getClientOrientation(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		if(o == null)
		{
			return null;
		}
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		return dp.getDirection();
	}

	public synchronized void removeClient(Client client) {
		assert(client != null);
		Object o = clientMap.remove(client);
		assert(o instanceof Point);
		Point point = (Point)o;
		if(point != null)
		{
			CellImpl cell = getCellImpl(point);
			cell.setContents(null);
		}
		clientMap.remove(client);
		clientFired.remove(client);
		client.unregisterMaze();
		client.removeClientListener(this);
		update();
		notifyClientRemove(client);
		
		Point p = this.getSize();
		for(int i = 0; i < p.getY(); i++) 
		{
			for(int j = 0; j < p.getX(); j++) 
			{
				CellImpl cell = (CellImpl)this.getCell(new Point(j,i));
				if(cell.getContents() == client)
				{
					cell.setContents(null);
				}
			}
		}
	}

	public synchronized boolean clientFire(Client client) {
		assert(client != null);
		// If the client already has a projectile in play
		// fail.
		if(clientFired.contains(client)) {
			return false;
		}
		if(clientMap.get(client) == null)
		{
			return false;
		}

		Point point = getClientPoint(client);
		Direction d = getClientOrientation(client);
		CellImpl cell = getCellImpl(point);

		/* Check that you can fire in that direction */
		if(cell.isWall(d)) {
			return false;
		}

		DirectedPoint newPoint = new DirectedPoint(point.move(d), d);
		/* Is the point withint the bounds of maze? */
		assert(checkBounds(newPoint));

		CellImpl newCell = getCellImpl(newPoint);
		Object contents = newCell.getContents();
		if(contents != null) {
			// If it is a Client, kill it outright
			if(contents instanceof Client) {
				notifyClientFired(client);
				killClient(client, (Client)contents);
				newCell.setContents(null);
				update();
				return true; 
			} else {
				// Otherwise fail (bullets will destroy each other)
				return false;
			}
		}

		clientFired.add(client);
		Projectile prj = new Projectile(client);

		/* Write the new cell */
		projectileMap.put(prj, newPoint);
		newCell.setContents(prj);
		notifyClientFired(client);
		update();
		return true; 
	}

	public synchronized boolean moveClientForward(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		if(o == null)
		{
			return false;
		}
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		return moveClient(client, dp.getDirection());
	}

	public synchronized boolean canMoveForward(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		if(o == null)
		{
			return false;
		}
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		return canMoveClient(client, dp.getDirection());
	}

	public synchronized boolean moveClientBackward(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		if(o == null)
		{
			return false;
		}
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		return moveClient(client, dp.getDirection().invert());
	}


	public synchronized Iterator getClients() {
		return clientMap.keySet().iterator();
	}


	public void addMazeListener(MazeListener ml) {
		listenerSet.add(ml);
	}

	public void removeMazeListener(MazeListener ml) {
		listenerSet.remove(ml);
	}

	/**
	 * Listen for notifications about action performed by 
	 * {@link Client}s in the maze.
	 * @param c The {@link Client} that acted.
	 * @param ce The action the {@link Client} performed.
	 */
	public void clientUpdate(Client c, ClientEvent ce) {
		// When a client turns, update our state.
		if(ce == ClientEvent.turnLeft) {
			rotateClientLeft(c);
		} else if(ce == ClientEvent.turnRight) {
			rotateClientRight(c);
		}
	}

	/* MazeImplDequeueThread calls this now. */
	public void handleProjectiles() {
		Collection deadPrj = new HashSet();
		//while(true) {
		if(!projectileMap.isEmpty()) {
			Iterator it = projectileMap.keySet().iterator();
			synchronized(projectileMap) {
				while(it.hasNext()) {   
					Object o = it.next();
					assert(o instanceof Projectile);
					
					Projectile prj = (Projectile)o;
					if(prj.remove == false)
					{
						deadPrj.addAll(moveProjectile((Projectile)o));
					}
				}               
				it = deadPrj.iterator();
				while(it.hasNext()) {
					Object o = it.next();
					assert(o instanceof Projectile);
					Projectile prj = (Projectile)o;
					DirectedPoint dp = (DirectedPoint)projectileMap.get(prj);
					projectileMap.remove(prj);
					clientFired.remove(prj.getOwner());
				}
				deadPrj.clear();
			}
		}
		/*
                        try {
                                dequeueThread.sleep(200);
                        } catch(Exception e) {
                                // shouldn't happen
                        }
		 */
		//}
	}
	
	private synchronized void quitClient(String client_name)
	{
		Client client = getClient(client_name);
	}
	
	/**
	 * Kill every client and clear all projectiles. This is done to sync
	 * everyone when a new client registers
	 */
	private synchronized void killEveryone()
	{
		Point p = this.getSize();
		for(int i = 0; i < p.getY(); i++) 
		{
			for(int j = 0; j < p.getX(); j++) 
			{
				CellImpl cell = (CellImpl)this.getCell(new Point(j,i));
				cell.setContents(null);
			}
		}
		projectileMap.clear();
		clientFired.clear();
		if(m_local_client != null)
		{
			Object o = clientMap.get(m_local_client);
			if(o != null)
			{
				m_local_client.set_last_known_location((DirectedPoint)o);
			}
		}
		if(m_bot_client != null)
		{
			Object o = clientMap.get(m_bot_client);
			if(o != null)
			{
				m_bot_client.set_last_known_location((DirectedPoint)o);
			}
		}
	}

	/* Internals */

	private synchronized Collection moveProjectile(Projectile prj) {
		Collection deadPrj = new LinkedList();
		assert(prj != null);

		Object o = projectileMap.get(prj);
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		Direction d = dp.getDirection();
		CellImpl cell = getCellImpl(dp);

		/* Check for a wall */
		if(cell.isWall(d)) {
			// If there is a wall, the projectile goes away.
			cell.setContents(null);
			deadPrj.add(prj);
			prj.remove = true;
			update();
			return deadPrj;
		}

		DirectedPoint newPoint = new DirectedPoint(dp.move(d), d);
		/* Is the point within the bounds of maze? */
		assert(checkBounds(newPoint));

		CellImpl newCell = getCellImpl(newPoint);
		Object contents = newCell.getContents();
		if(contents != null) {
			// If it is a Client, kill it outright
			if(contents instanceof Client) {
				killClient(prj.getOwner(), (Client)contents);
				cell.setContents(null);
				newCell.setContents(null);
				deadPrj.add(prj);
				prj.remove = true;
				update();
				return deadPrj;
			} else {
				// Bullets destroy each other
				assert(contents instanceof Projectile);
				newCell.setContents(null);
				cell.setContents(null);
				deadPrj.add(prj);
				deadPrj.add(contents);
				prj.remove = true;
				Projectile otherprj = (Projectile)contents;
				otherprj.remove = true;
				update();
				return deadPrj;
			}
		}

		/* Clear the old cell */
		cell.setContents(null);
		/* Write the new cell */
		projectileMap.put(prj, newPoint);
		newCell.setContents(prj);
		update();
		return deadPrj;
	}
	/**
	 * Internal helper for adding a {@link Client} to the {@link Maze}.
	 * @param client The {@link Client} to be added.
	 * @param point The location the {@link Client} should be added.
	 */
	/*
        private synchronized void addClient(Client client, Point point) {
                assert(client != null);
                assert(checkBounds(point));
                CellImpl cell = getCellImpl(point);
                Direction d = Direction.random();
                while(cell.isWall(d)) {
                  d = Direction.random();
                }
                cell.setContents(client);
                clientMap.put(client, new DirectedPoint(point, d));
                client.registerMaze(this);
                client.addClientListener(this);
                update();
                notifyClientAdd(client);
        }
	 */

	/**
	 * Internal helper for handling the death of a {@link Client}.
	 * @param source The {@link Client} that fired the projectile.
	 * @param target The {@link Client} that was killed.
	 */
	private synchronized void killClient(Client source, Client target) {
		assert(source != null);
		assert(target != null);
		Mazewar.consolePrintLn(source.getName() + " just vaporized " + 
				target.getName());
		Object o = clientMap.get(target);
		assert(o instanceof Point);
		Point point = (Point)o;
		CellImpl cell = getCellImpl(point);
		cell.setContents(null);
		if(target instanceof LocalClient){
			((LocalClient)target).spawn();
		} else if (target instanceof RobotClient) {
			((RobotClient)target).spawn();
		}
		/*
                // Pick a random starting point, and check to see if it is already occupied
                point = new Point(randomGen.nextInt(maxX),randomGen.nextInt(maxY));
                cell = getCellImpl(point);
                // Repeat until we find an empty cell
                while(cell.getContents() != null) {
                        point = new Point(randomGen.nextInt(maxX),randomGen.nextInt(maxY));
                        cell = getCellImpl(point);
                }
                Direction d = Direction.random();
                while(cell.isWall(d)) {
                        d = Direction.random();
                }
                cell.setContents(target);
                clientMap.put(target, new DirectedPoint(point, d));
                update();
		 */
		clientMap.put(target, null);
		notifyClientKilled(source, target);
	}

	/**
	 * Internal helper called when a {@link Client} emits a turnLeft action.
	 * @param client The {@link Client} to rotate.
	 */
	private synchronized void rotateClientLeft(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		if(o == null)
		{
			return;
		}
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		clientMap.put(client, new DirectedPoint(dp, dp.getDirection().turnLeft()));
		update();
	}

	/**
	 * Internal helper called when a {@link Client} emits a turnRight action.
	 * @param client The {@link Client} to rotate.
	 */
	private synchronized void rotateClientRight(Client client) {
		assert(client != null);
		Object o = clientMap.get(client);
		if(o == null)
		{
			return;
		}
		assert(o instanceof DirectedPoint);
		DirectedPoint dp = (DirectedPoint)o;
		clientMap.put(client, new DirectedPoint(dp, dp.getDirection().turnRight()));
		update();
	}

	/**
	 * Internal helper called to move a {@link Client} in the specified
	 * {@link Direction}.
	 * @param client The {@link Client} to be move.
	 * @param d The {@link Direction} to move.
	 * @return If the {@link Client} cannot move in that {@link Direction}
	 * for some reason, return <code>false</code>, otherwise return 
	 * <code>true</code> indicating success.
	 */
	private synchronized boolean moveClient(Client client, Direction d) {
		assert(client != null);
		assert(d != null);
		Point oldPoint = getClientPoint(client);
		CellImpl oldCell = getCellImpl(oldPoint);

		/* Check that you can move in the given direction */
		if(oldCell.isWall(d)) {
			/* Move failed */
			clientMap.put(client, oldPoint);
			return false;
		}

		DirectedPoint newPoint = new DirectedPoint(oldPoint.move(d), getClientOrientation(client));

		/* Is the point withint the bounds of maze? */
		assert(checkBounds(newPoint));
		CellImpl newCell = getCellImpl(newPoint);
		if(newCell.getContents() != null) {
			/* Move failed */
			clientMap.put(client, oldPoint);
			return false;
		}

		/* Write the new cell */
		clientMap.put(client, newPoint);
		newCell.setContents(client);
		/* Clear the old cell */
		oldCell.setContents(null);	

		update();
		return true; 
	}

	private synchronized boolean canMoveClient(Client client, Direction d) {
                assert(client != null);
                assert(d != null);
                Point oldPoint = getClientPoint(client);
                CellImpl oldCell = getCellImpl(oldPoint);

                /* Check that you can move in the given direction */
                if(oldCell.isWall(d)) {
                        /* Move failed */
                        clientMap.put(client, oldPoint);
                        return false;
                }

                DirectedPoint newPoint = new DirectedPoint(oldPoint.move(d), getClientOrientation(client));

                /* Is the point withint the bounds of maze? */
                assert(checkBounds(newPoint));
                CellImpl newCell = getCellImpl(newPoint);
                if(newCell.getContents() != null) {
                        /* Move failed */
                        clientMap.put(client, oldPoint);
                        return false;
                }

                return true;
        }
	
	// packetQueueLock MUST be acquired before calling this
	// function
	private EventQueueElement pollPacketQueueHead()
	{
		LamportClock c = m_lclock_queue.poll();
		if(c == null)
		{
			return null;
		}
		
		EventQueueElement e = m_clock_to_event_map.get(c);
		if(e == null)
		{
			throw new UnknownError("LamportClock " + c.toString() + " did not have a corresponding entry in the event map");
		}
		
		if(e.message() == null)
		{
			throw new UnknownError("LamportClock " + c.toString() + " message field was null");
		}
		if(!e.isAckConsistent())
		{
			new UnknownError("LamportClock " + c.toString() + " num acks is negative!").printStackTrace();
		}
		
		return e;
	}
	
	// packetQueueLock MUST be acquired before calling this
	// function
	private EventQueueElement peekPacketQueueHead()
	{
		LamportClock c = m_lclock_queue.peek();
		if(c == null)
		{
			return null;
		}
		
		EventQueueElement e = m_clock_to_event_map.get(c);
		if(e == null)
		{
			throw new UnknownError("LamportClock " + c.toString() + " did not have a corresponding entry in the event map");
		}
		
		if(e.message() == null)
		{
			throw new UnknownError("LamportClock " + c.toString() + " message field was null");
		}
		if(!e.isAckConsistent())
		{
			new UnknownError("LamportClock " + c.toString() + " num acks is negative! MessageType is " +
					e.message().typestr()).printStackTrace();
		}
		
		return e;
	}
	
	// packetQueueLock MUST be acquired before calling this
	// function
	private boolean packetQueueHeadIsReady()
	{
		// Get the LamportClock of the element at the Head of the
		// queue
		EventQueueElement e = peekPacketQueueHead();
		if(e == null)
		{
			return false;
		}
		
		if(e.ready())
		{
			return true;
		}
		else
		{
			//System.out.println("Info: " + e.message().typestr() + " is not ready");
		}
		
		return false;
	}

	public EventQueueElement dequeue(){
		EventQueueElement e = null;

		packetQueueLock.lock();
		try{
			while(!packetQueueHeadIsReady()){
				try{
					packetQueueEmpty.await();
				} catch(InterruptedException err){
					System.out.println("Warning: MazeImpl: dequeue: InterruptedException.");
				}
			}

			e = pollPacketQueueHead();
		} finally{
			packetQueueLock.unlock();
		}

		return e;
	}
	
	private synchronized void broadcastPacket(MessagePacket m)
	{
		assert(m.lclock != null);
		ArrayList<ClientInfo> client_list = get_client_list();
		for(ClientInfo c : client_list)
		{
			synchronized(c.ostream)
			{
				try
				{
					c.ostream.writeObject(m);
				}
				catch(Exception e)
				{
					System.out.println("Warning: Could not send packet of type " + m.typestr() +
							" to " + c.hostname + ":" + c.port);
				}
			}
		}
	}

	/**
	 * Multicast an event generated by the local client to all connected clients. This involves
	 * broadcasting the event, its ack and adding the event to our priority queue
	 */
	public synchronized void multicastEvent(MessagePacket m, boolean sendAck)
	{
		// This is only for events that are meant to be queued
		// Acks should be sent to the 'handle_ack' function
		assert(m.type != MessagePacket.CLIENT_ACK);
		m.id = m_lclock.id();

		if(m.type == MessagePacket.CLIENT_EXIT)
		{
			m_exit_protocol_initiated = true;
		}
		else if(m_exit_protocol_initiated && (m.type != MessagePacket.CLIENT_EXIT_DONE))
		{
			return;
		}
		
		packetQueueLock.lock();
		try
		{
			m_lclock.issue();
			EventQueueElement e = new EventQueueElement(m.type);
			e.setMessage(m, get_client_list_size());
			LamportClock clock = new LamportClock(m_lclock);
			if(m_lclock_queue.contains(clock))
			{
				throw new UnknownError("Lamport Clock already existed in queue for a newly issued local event!");
			}
			m_lclock_queue.add(clock);
			if(m_clock_to_event_map.containsKey(clock))
			{
				throw new UnknownError("Lamport clock already existed in map for a newly issued local event!");
			}
			m_clock_to_event_map.put(clock, e);
			m.lclock = clock;
			//System.out.println("Info: MCAST Queue: " + m.lclock.toString() + ", acks: " + e.num_acks());
			m_lclock.send();
			broadcastPacket(m);
			packetQueueEmpty.signal();
			
			// Now send the ack if required
			if(sendAck)
			{
				MessagePacket ack = new MessagePacket();
				ack.id = m_lclock.id();
				ack.type = MessagePacket.CLIENT_ACK;
				ack.ack_type = m.type;
				ack.lclock = clock;
				broadcastPacket(ack);
			}
		}
		finally
		{
			packetQueueLock.unlock();
		}
	}
	

	public synchronized void enqueueEvent(MessagePacket m,
			ObjectInputStream istream,
			ObjectOutputStream ostream,
			Socket socket)
	{
		if(m.lclock.id() == m_lclock.id())
		{
			// This could also happen if we try to handle our own ack in this function
			throw new UnknownError("The LamportClock id of an externally generated" + 
				" event was the same as our ID! " +
				"Message: " + m.typestr() + ", " + m.lclock.toString() );
		}
		
		// Send the ACK.
		MessagePacket ack = new MessagePacket();
		ack.id = m_lclock.id();
		ack.type = MessagePacket.CLIENT_ACK;
		ack.ack_type = m.type;
		ack.lclock = m.lclock;
		broadcastPacket(ack);
		
		packetQueueLock.lock();
		// Now add the packet to our priority queue
		try
		{
			if(m_lclock_queue.contains(m.lclock))
			{
				// A LamportClock for an event that hasn't arrived
				// yet can't already be in the queue. Either we messed
				// up in handle_ack or some client is being naughty and not
				// incrementing their LamportClock properly
				new UnknownError("The LamportClock for a newly received event already existed in the queue!");
			}
			else
			{
				EventQueueElement e = new EventQueueElement(m.type);
				// If the event map already has the lamportclock, it
				// means an ack arrived for this event before the actual
				// event
				if(m_clock_to_event_map.containsKey(m.lclock))
				{
					e = m_clock_to_event_map.get(m.lclock);
					assert(e.type() == m.type);
				}
				e.setMessage(m, get_client_list_size());
				//System.out.println("Info: Queue: " + m.lclock.toString() + ", acks: " + e.num_acks());
				e.istream = istream;
				e.ostream = ostream;
				e.socket = socket;
				m_lclock_queue.add(m.lclock);
				m_clock_to_event_map.put(m.lclock, e);
				// Don't think this is necessary
				packetQueueEmpty.signal();
			}
		}
		finally
		{
			packetQueueLock.unlock();
		}
	}
	
	public synchronized void handle_ack(MessagePacket m)
	{
		if(m.id == -1)
		{
			new UnknownError("Someone sent an ACK without their id!").printStackTrace();
		}
		packetQueueLock.lock();
		try
		{
			// If the Lamport clock is not in the
			// queue, it means we got the ack for
			// an event before the actual event
			if(!m_lclock_queue.contains(m.lclock))
			{
				EventQueueElement e;
				// This is the first ack for this event because
				// neither the priority queue nor the event map
				// have this LamportClock
				if(!m_clock_to_event_map.containsKey(m.lclock))
				{
					e = new EventQueueElement(m.ack_type);
					// -1 because we are accounting for this
					// ack
					e.ack(m.id);
					m_clock_to_event_map.put(m.lclock, e);	
					packetQueueEmpty.signal();
				}
				// We already received an ack for this event
				// because the LamportClock is in the hashmap.
				// We haven't received the actual event because
				// the clock is not in the queue
				else
				{
					e = m_clock_to_event_map.get(m.lclock);
					e.ack(m.id);
					packetQueueEmpty.signal();
				}
			}
			else
			{
				// Got an ack, just decrement the ACK count
				EventQueueElement e = m_clock_to_event_map.get(m.lclock);
				e.ack(m.id);
				packetQueueEmpty.signal();
			}
		}
		finally
		{
			packetQueueLock.unlock();
		}
	}
	
	public void handle_client_quitting(EventQueueElement e)
	{
		if(e.message().id == m_lclock.id())
		{
			// No need to release the coordination lock. The coordination
			// server automatically releases the lock when we exit
			//release_coordination_lock();
			try
			{
				Thread.sleep(1000);
			}
			catch(InterruptedException err)
			{
				
			}
			System.exit(0);
		}
		System.out.println("Info: handle_client_quitting: Attempting to acquire packet queue lock");
		packetQueueLock.lock();
		System.out.println("Info: handle_client_quitting: Packet queue lock acquired");
		try
		{
			ArrayList<LamportClock> lclock_list = new ArrayList<LamportClock>();
			for(LamportClock iclock : m_clock_to_event_map.keySet())
			{
				lclock_list.add(new LamportClock(iclock));
			}
			for(LamportClock iclock : lclock_list)
			{
				if(iclock.id() == e.message().id)
				{
					m_clock_to_event_map.remove(iclock);
					m_lclock_queue.remove(iclock);
				}
				else
				{
					m_clock_to_event_map.get(iclock).ack(e.message().id);
				}
			}
		}
		finally
		{
			packetQueueLock.unlock();
		}
		
		remove_client(e.message().id);
		Client client_to_remove = getClient(e.message().client_name);
		// also remove bot client associated with the client that's quitting
		Client bot_to_remove = getClient(e.message().client_name + "_bot");
		if(client_to_remove != null)
		{
			removeClient(client_to_remove);
		}
		if(bot_to_remove != null)
		{
			removeClient(bot_to_remove);
		}
		Mazewar.consolePrintLn(e.message().client_name + " has left the game!");
        Mazewar.consolePrintLn(e.message().client_name + "_bot has left the game!");
	}
	
	public void handle_new_client_registration(EventQueueElement e)
	{
		// Now clear our packet queue
		packetQueueLock.lock();
		try
		{
			m_lclock_queue.clear();
			m_clock_to_event_map.clear();
		}
		finally
		{
			packetQueueLock.unlock();
		}
		
		// Now send the ack for the CLIENT_REG to the new client. We didn't ack them
		// when we received the packet because they were not on our client list at
		// that time.
		// Add the client to our list if it is not us
		if(e.message().id != m_lclock.id())
		{
			ClientInfo c = new ClientInfo();
			c.id = e.message().id;
			c.hostname = e.socket.getInetAddress().getHostAddress();
			c.port = e.message().port;
			c.socket = e.socket;
			c.ostream = e.ostream;
			c.istream = e.istream;
			add_client(c);
			MessagePacket ack = new MessagePacket();
			ack.type = MessagePacket.CLIENT_ACK;
			ack.ack_type = MessagePacket.CLIENT_REG;
			ack.id = m_lclock.id();
			ack.lclock = e.message().lclock;
			try 
			{
				synchronized(c.ostream)
				{
					c.ostream.writeObject(ack);
				}
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
				System.exit(-1);
			}
		}
		
		// Finally kill everything currently on the maze
		killEveryone();
		
		// If this registration is for us, multicast a REG_DONE
		// event.
		if(e.message().id == m_lclock.id())
		{
			MessagePacket m = new MessagePacket();
			m.type = MessagePacket.CLIENT_REG_DONE;
			m.id = m_lclock.id();
			// We need to explicitly tell the other clients
			// how many acks to expect for this packet because
			// some of them might not have registered us yet
			m.num_packets = get_client_list_size();
			multicastEvent(m, true);
		}
	}

	public void getFromServer() throws IOException, ClassNotFoundException {
		assert(false);
	}
	
	public LamportClock getLClock()
	{
		return m_lclock;
	}

	/* Get a client given a client name */
	public Client getClient(String clientName){
		Iterator it = getClients();
		Client client = null;
		synchronized(clientMap) {
			while(it.hasNext()) {
				Object o = it.next();
				assert(o instanceof Client);
				if(((Client)o).getName().equals(clientName)){
					client = (Client)o;
					break;
				}
			}
		}
		return client;
	}

	/* Cleanup before Mazewar quits */
	public void cleanup(){
		// Just kill everything
		System.exit(0);
	}

	/**
	 * Generate a notification to listeners that a
	 * {@link Client} has been added.
	 * @param c The {@link Client} that was added.
	 */
	private void notifyClientAdd(Client c) {
		assert(c != null);
		Iterator i = listenerSet.iterator();
		while (i.hasNext()) {
			Object o = i.next();
			assert(o instanceof MazeListener);
			MazeListener ml = (MazeListener)o;
			ml.clientAdded(c);
		} 
	}

	/**
	 * Generate a notification to listeners that a 
	 * {@link Client} has been removed.
	 * @param c The {@link Client} that was removed.
	 */
	private void notifyClientRemove(Client c) {
		assert(c != null);
		Iterator i = listenerSet.iterator();
		while (i.hasNext()) {
			Object o = i.next();
			assert(o instanceof MazeListener);
			MazeListener ml = (MazeListener)o;
			ml.clientRemoved(c);
		} 
	}

	/**
	 * Generate a notification to listeners that a
	 * {@link Client} has fired.
	 * @param c The {@link Client} that fired.
	 */
	private void notifyClientFired(Client c) {
		assert(c != null);
		Iterator i = listenerSet.iterator();
		while (i.hasNext()) {
			Object o = i.next();
			assert(o instanceof MazeListener);
			MazeListener ml = (MazeListener)o;
			ml.clientFired(c);
		} 
	}

	/**
	 * Generate a notification to listeners that a
	 * {@link Client} has been killed.
	 * @param source The {@link Client} that fired the projectile.
	 * @param target The {@link Client} that was killed.
	 */
	private void notifyClientKilled(Client source, Client target) {
		assert(source != null);
		assert(target != null);
		Iterator i = listenerSet.iterator();
		while (i.hasNext()) {
			Object o = i.next();
			assert(o instanceof MazeListener);
			MazeListener ml = (MazeListener)o;
			ml.clientKilled(source, target);
		} 
	}

	/**
	 * Generate a notification that the {@link Maze} has 
	 * changed in some fashion.
	 */
	private void update() {
		Iterator i = listenerSet.iterator();
		while (i.hasNext()) {
			Object o = i.next();
			assert(o instanceof MazeListener);
			MazeListener ml = (MazeListener)o;
			ml.mazeUpdate();
		} 
	}

	/**
	 * A concrete implementation of the {@link Cell} class that
	 * special to this implementation of {@link Maze}s.
	 */
	private class CellImpl extends Cell implements Serializable {
		/**
		 * Has this {@link CellImpl} been visited while
		 * constructing the {@link Maze}.
		 */
		private boolean visited = false;

		/**
		 * The walls of this {@link Cell}.
		 */
		private boolean walls[] = {true, true, true, true};

		/**
		 * The contents of the {@link Cell}. 
		 * <code>null</code> indicates that it is empty.
		 */
		private Object contents = null;

		/**
		 * Helper function to convert a {@link Direction} into 
		 * an array index for easier access.
		 * @param d The {@link Direction} to convert.
		 * @return An integer index into <code>walls</code>.
		 */
		private int directionToArrayIndex(Direction d) {
			assert(d != null);
			if(d.equals(Direction.North)) {
				return 0;
			} else if(d.equals(Direction.East)) {
				return 1;
			} else if(d.equals(Direction.South)) {
				return 2;
			} else if(d.equals(Direction.West)) {
				return 3;
			}
			/* Impossible */
			return -1; 
		}

		/* Required for the abstract implementation */

		public boolean isWall(Direction d) {
			assert(d != null);
			return this.walls[directionToArrayIndex(d)];
		}

		public synchronized Object getContents() {
			return this.contents;
		}

		/* Internals used by MazeImpl */

		/**
		 * Indicate that this {@link Cell} has been
		 * visited while building the {@link MazeImpl}.
		 */
		public void setVisited() {
			visited = true;
		}

		/**
		 * Has this {@link Cell} been visited in the process
		 * of recursviely building the {@link Maze}?
		 * @return <code>true</code> if visited, <code>false</code> 
		 * otherwise.
		 */
		public boolean visited() {
			return visited;
		}

		/**
		 * Add a wall to this {@link Cell} in the specified
		 * Cardinal {@link Direction}.
		 * @param d Which wall to add.
		 */
		public void setWall(Direction d) {
			assert(d != null);
			this.walls[directionToArrayIndex(d)] = true;
		}

		/**
		 * Remove the wall from this {@link Cell} in the specified
		 * Cardinal {@link Direction}.
		 * @param d Which wall to remove.
		 */
		public void removeWall(Direction d) {
			assert(d != null);
			this.walls[directionToArrayIndex(d)] = false;
		}

		/**
		 * Set the contents of this {@link Cell}.
		 * @param contents Object to place in the {@link Cell}.
		 * Use <code>null</code> if you want to empty it.
		 */
		public synchronized void setContents(Object contents) {
			this.contents = contents;
		}

	}

	/** 
	 * Removes the wall in the {@link Cell} at the specified {@link Point} and 
	 * {@link Direction}, and the opposite wall in the adjacent {@link Cell}.
	 * @param point Location to remove the wall.
	 * @param d Cardinal {@link Direction} specifying the wall to be removed.
	 */
	private void removeWall(Point point, Direction d) {
		assert(point != null);
		assert(d != null);
		CellImpl cell = getCellImpl(point);
		cell.removeWall(d);
		Point adjacentPoint = point.move(d);
		CellImpl adjacentCell = getCellImpl(adjacentPoint);
		adjacentCell.removeWall(d.invert());
	}

	/** 
	 * Pick randomly pick an unvisited neighboring {@link CellImpl}, 
	 * if none return <code>null</code>. 
	 * @param point The location to pick a neighboring {@link CellImpl} from.
	 * @return The Cardinal {@link Direction} of a {@link CellImpl} that hasn't
	 * yet been visited.
	 */
	private Direction pickNeighbor(Point point) {
		assert(point != null);
		Direction directions[] = { 
				Direction.North, 
				Direction.East, 
				Direction.West, 
				Direction.South };

		// Create a vector of the possible choices
		Vector options = new Vector();	       

		// Iterate through the directions and see which
		// Cells have been visited, adding those that haven't
		for(int i = 0; i < 4; i++) {
			Point newPoint = point.move(directions[i]);
			if(checkBounds(newPoint)) {
				CellImpl cell = getCellImpl(newPoint);
				if(!cell.visited()) {
					options.add(directions[i]);
				}
			}
		}

		// If there are no choices just return null
		if(options.size() == 0) {
			return null;
		}

		// If there is at least one option, randomly choose one.
		int n = randomGen.nextInt(options.size());

		Object o = options.get(n);
		assert(o instanceof Direction);
		return (Direction)o;
	}

	/**
	 * Recursively carve out a {@link Maze}
	 * @param point The location in the {@link Maze} to start carving.
	 */
	private void buildMaze(Point point) {
		assert(point != null);
		CellImpl cell = getCellImpl(point);
		cell.setVisited();
		Direction d = pickNeighbor(point);
		while(d != null) {	    
			removeWall(point, d);
			Point newPoint = point.move(d);
			buildMaze(newPoint);
			d = pickNeighbor(point);
		}
	}

	/** 
	 * Obtain the {@link CellImpl} at the specified point. 
	 * @param point Location in the {@link Maze}.
	 * @return The {@link CellImpl} representing that location.
	 */
	private CellImpl getCellImpl(Point point) {
		assert(point != null);
		Object o1 = mazeVector.get(point.getX());
		assert(o1 instanceof Vector);
		Vector v1 = (Vector)o1;
		Object o2 = v1.get(point.getY());
		assert(o2 instanceof CellImpl);
		return (CellImpl)o2;
	}
	
	/**
	 * The random number generator used by the {@link Maze}.
	 */
	private final Random randomGen;

	/**
	 * The maximum X coordinate of the {@link Maze}.
	 */
	private final int maxX;

	/**
	 * The maximum Y coordinate of the {@link Maze}.
	 */
	private final int maxY;

	/** 
	 * The {@link Vector} of {@link Vector}s holding the
	 * {@link Cell}s of the {@link Maze}.
	 */
	private final Vector mazeVector;

	/**
	 * A map between {@link Client}s and {@link DirectedPoint}s
	 * locating them in the {@link Maze}.
	 */
	private final Map clientMap = new HashMap();

	/**
	 * The set of {@link MazeListener}s that are presently
	 * in the notification queue.
	 */
	private final Set listenerSet = new HashSet();

	/**
	 * Mapping from {@link Projectile}s to {@link DirectedPoint}s. 
	 */
	private final Map projectileMap = new HashMap();

	/**
	 * The set of {@link Client}s that have {@link Projectile}s in 
	 * play.
	 */
	private final Set clientFired = new HashSet();

	/**
	 * The thread used to dequeue message packets (and process
	 * them accordingly).
	 */
	private final Thread dequeueThread;

	/**
	 * The bullet ticker thread. Only one client in the game will
	 * have this thread active.
	 */
	private Thread bulletTickerThread = null;
	private ReentrantLock bulletTickerLock = null;

	/**
	 * The local client. Can be set to a non-null value only once
	 */
	private LocalClient m_local_client = null;

	/**
         * The local bot client. Can be set to a non-null value only once
         */
	private RobotClient m_bot_client = null;
	
	private LamportClock m_lclock;
	
	/**
	 * Command line options for client
	 */
	private ArgumentParser m_opts;
	
	/**
	 * This thread will listen for new connections
	 * from other clients
	 */
	private Thread m_listener_thread;
	
	/**
	 * List of clients connected to our game along
	 * with their connection information
	 */
	private ArrayList<ClientInfo> m_client_list;
	private ReentrantLock m_client_list_lock;
	
	/**
	 * Queue of currently unprocessed events which
	 * are waiting for their ACKs.
	 */
	private PriorityQueue<LamportClock> m_lclock_queue;
	private HashMap<LamportClock, EventQueueElement> m_clock_to_event_map;
	
	/**
	 * Synchronization primitives for the event queue
	 */
	private ReentrantLock packetQueueLock;	
	private Condition packetQueueEmpty;
	
	/**
	 * Data members for coordination lock acquire and release
	 * from central coordination server
	 */
	private boolean m_have_coordination_lock;
	private Socket m_coordination_server_socket;
	private ObjectOutputStream m_coordination_server_ostream;
	private ObjectInputStream m_coordination_server_istream;
	private ReentrantLock m_local_coordination_lock;

	private ScoreTableModel m_score_table;

	private boolean m_exit_protocol_initiated;
}
