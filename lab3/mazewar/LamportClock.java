import java.io.Serializable;

public class LamportClock implements Serializable {
	public LamportClock(long id)
	{
		m_clock = new Long(0);
		m_id = new Long(id);
	}
	
	public LamportClock(long id, LamportClock clock)
	{
		if(clock.m_clock < 0)
		{
			throw new IllegalArgumentException("Lamport clock out of range: " + clock.m_clock);
		}
		m_clock = new Long(clock.m_clock);
		m_id = new Long(id);
	}
	public LamportClock(LamportClock clock)
	{
		m_clock = new Long(clock.m_clock);
		m_id = new Long(clock.m_id);
	}
	
	public synchronized long clock()
	{
		return m_clock.longValue();
	}
	
	public synchronized long id()
	{
		return m_id.longValue();
	}
	
	public synchronized long issue()
	{
		m_clock++;
		return m_clock.longValue();
	}
	
	public synchronized long send()
	{
		m_clock++;
		return m_clock.longValue();
	}
	
	public synchronized long increment(long offset)
	{
		m_clock += offset;
		return m_clock.longValue();
	}
	
	public synchronized long receive(long clock)
	{
		if(clock > m_clock)
		{
			m_clock = clock;
		}
		m_clock++;
		return m_clock.longValue();
	}
	
	public synchronized String toString()
	{
		return new String("(" + m_id + ", " + m_clock + ")");
	}
	
	@Override
	public synchronized int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_clock == null) ? 0 : m_clock.hashCode());
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		return result;
	}

	@Override
	public synchronized boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LamportClock other = (LamportClock) obj;
		if (m_clock == null) {
			if (other.m_clock != null)
				return false;
		} else if (!m_clock.equals(other.m_clock))
			return false;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		return true;
	}

	private Long m_clock;
	private Long m_id;
}
