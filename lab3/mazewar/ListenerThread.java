import java.net.*;
import java.io.*;

public class ListenerThread extends Thread {
	public ListenerThread(Maze maze, ArgumentParser opts)
	{
		m_maze = maze;
		m_opts = opts;
	}
	
	public void run()
	{
		// Try starting a server socket on the listen port
		ServerSocket socket = null;
		try
		{
			socket = new ServerSocket(m_opts.listenport());
		}
		catch(IOException e)
		{
			System.err.println("Error: Failed to listen at specified port. Got IO exception");
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
		catch(IllegalArgumentException e)
		{
			System.err.println("Error: Failed to listen at specified port. Got IllegalArgument exception");
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
		System.out.println("Info: Listening for incoming connections on " + socket.getInetAddress().getHostAddress() +
				":" + socket.getLocalPort());
		Socket clientsock;
		while(!interrupted())
		{
			try
			{
				while((clientsock = socket.accept()) != null)
				{
					ObjectInputStream istream = new ObjectInputStream(clientsock.getInputStream());
					ObjectOutputStream ostream = new ObjectOutputStream(clientsock.getOutputStream());
					new NewClientHandlerThread(m_maze, clientsock, istream, ostream).start();
				}
			}
			catch(IOException e)
			{
				// ignore failed connection attempts
			}
			System.out.println("Info: " + getClass().getSimpleName() + " is going AWOL");
		}
	}
	
	Maze m_maze;
	ArgumentParser m_opts;
}
