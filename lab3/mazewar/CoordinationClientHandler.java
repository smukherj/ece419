import java.net.*;
import java.util.concurrent.locks.ReentrantLock;
import java.io.*;

public class CoordinationClientHandler extends Thread {

	public CoordinationClientHandler(Socket socket, ReentrantLock lock) {
		m_socket = socket;
		m_lock = lock;
	}
	
	public void run()
	{
		System.out.println("Info: Got a connection from " + m_socket.getInetAddress().getHostName() +
				":" + m_socket.getPort());
		ObjectOutputStream ostream = null;
		ObjectInputStream istream = null;
		String name = "<unknown>";
		try 
		{
			ostream = new ObjectOutputStream(m_socket.getOutputStream());
			istream = new ObjectInputStream(m_socket.getInputStream());
			MessagePacket m;
			while((m = (MessagePacket)istream.readObject()) != null)
			{
				if(m.client_name != null && !m.client_name.equals("<unknown>"))
				{
					name = m.client_name;
				}
				if(m.type == MessagePacket.COORD_LOCK_ACQUIRE)
				{
					m_lock.lock();
					try
					{
						Thread.sleep(1000);
					}
					catch(InterruptedException e)
					{
						
					}
					System.out.println("Info: Granting lock to " + m.client_name);
					m = new MessagePacket();
					m.type = MessagePacket.COORD_LOCK_ACK;
					ostream.writeObject(m);
				}
				else if(m.type == MessagePacket.COORD_LOCK_REL)
				{
					m_lock.unlock();
					System.out.println("Info: " + m.client_name + " has released the lock");
					m = new MessagePacket();
					m.type = MessagePacket.COORD_LOCK_ACK;
					ostream.writeObject(m);
				}
				else
				{
					System.out.println("Error: Unrecognized packet of type " + m.typestr() + " from " + name);
				}
			}
		} 
		catch (IOException e) 
		{
		}
		catch(ClassNotFoundException e)
		{			
		}
		finally
		{
			if(m_lock.isHeldByCurrentThread())
			{
				System.out.println("Info: Force releasing lock held by " + name);
				m_lock.unlock();
			}
		}
		System.out.println("Info: Closing connection with " + name);
	}
	
	Socket m_socket;
	ReentrantLock m_lock;
}
