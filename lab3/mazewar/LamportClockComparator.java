import java.util.Comparator;


public class LamportClockComparator implements Comparator<LamportClock> {

	@Override
	public int compare(LamportClock arg0, LamportClock arg1) {
		if(arg0 == arg1)
		{
			return 0;
		}
		else if(arg0.clock() != arg1.clock())
		{
			return (int)(arg0.clock() - arg1.clock());
		}
		else
		{
			return (int)(arg0.id() - arg1.id());
		}
	}

}
