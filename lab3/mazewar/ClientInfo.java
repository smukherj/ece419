import java.io.*;
import java.net.*;


public class ClientInfo {
	/**
	 * These are the hostname and port of the ServerSocket the
	 * client is listening on for new connections. The id is
	 * unique to each client.
	 */
	public String hostname;
	public int port;
	public long id;
	
	/**
	 * These are the connection details for the private TCP
	 * connection between the local client and the other client.
	 */
	public Socket socket;
	public ObjectOutputStream ostream;
	public ObjectInputStream istream;
	
	public ClientInfo()
	{
		hostname = null;
		port = -1;
		id = -1;
		socket = null;
		ostream = null;
		istream = null;
	}
	
	public ClientInfo(ClientInfo c)
	{
		hostname = c.hostname;
		port = c.port;
		id = c.id;
		socket = c.socket;
		ostream = c.ostream;
		istream = c.istream;
	}
}
