/*
Copyright (C) 2004 Geoffrey Alan Washburn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
 */

import java.util.Random;
import java.util.Vector;
import java.util.concurrent.locks.ReentrantLock;
import java.lang.Runnable;
import java.lang.Thread;
import java.io.*;
import java.net.*;

/**
 * A very naive implementation of a computer controlled {@link LocalClient}.  Basically
 * it stumbles about and shoots.  
 * @author Geoffrey Washburn &lt;<a href="mailto:geoffw@cis.upenn.edu">geoffw@cis.upenn.edu</a>&gt;
 * @version $Id: RobotClient.java 345 2004-01-24 03:56:27Z geoffw $
 */

public class RobotClient extends Client implements Runnable {

	/**
	 * Random number generator so that the robot can be
	 * "non-deterministic".
	 */ 
	private final Random randomGen = new Random();

	/**
	 * The {@link Thread} object we use to run the robot control code.
	 */
	private final Thread thread;

	private boolean started = false; 
	private boolean active = false;
	private boolean permanent_deactivation = false;
	private DirectedPoint m_last_known_location;

	/**
	 * Create a computer controlled {@link LocalClient}.
	 * @param name The name of this {@link RobotClient}.
	 */
	public RobotClient(String name) {
		super(name);
		assert(name != null);
		// Create our thread
		thread = new Thread(this);
		m_last_known_location = null;
	}
	
	public void set_last_known_location(DirectedPoint dp)
	{
		m_last_known_location = dp;
	}
	
	private DirectedPoint get_last_known_location()
	{
		DirectedPoint dp = null;
		if(m_last_known_location != null)
		{
			dp = m_last_known_location;
			m_last_known_location = null;
		}
		return dp;
	}

	public synchronized void spawn(){
		
		MessagePacket spawnPacket = new MessagePacket();

		spawnPacket.type = MessagePacket.CLIENT_SPAWN;
		spawnPacket.client_name = getName();
		
		DirectedPoint dp = get_last_known_location();
		if(dp != null)
		{
			spawnPacket.point = dp;
			spawnPacket.dir = dp.getDirection();
		}
		else
		{
			spawnPacket.point = maze.getSpawnPoint();
			spawnPacket.dir = maze.getSpawnDirection(spawnPacket.point);
		}
		spawnPacket.score = maze.get_bot_score();
		maze.multicastEvent(spawnPacket, true);
	}

	public boolean started(){
		return started;
	}

	public void start(){
		thread.start();
		started = true;
	}

	public void activate() {
		if(permanent_deactivation)
		{
			System.out.println("Info: Rejecting Robot Client activation request");
			return;
		}
		active = true;
		System.out.println("Info: Robot Client activated");
	}

	public void pause() {
		active = false;
		System.out.println("Info: Robot Client deactivated");
	}
	
	public void permanently_deactivate()
	{
		permanent_deactivation = true;
		System.out.println("Info: Robot Client permanently deactivated");
	}
	
	public void wait_until_exit()
	{
	}


	/** 
	 * This method is the control loop for an active {@link RobotClient}. 
	 */
	public void run() {
		System.out.println("Info: RobotClient starting.");

		//hack: give some time for spawn
		try {
			Thread.sleep(500);
		} catch(Exception e) {
			// Shouldn't happen.
		}

		// Loop while we are active
		while(!permanent_deactivation) {
			while(!active && !permanent_deactivation){
				try {
					Thread.sleep(500);
				} catch(Exception e) {
					// Shouldn't happen.
				}
			}

			MessagePacket packetToServer = new MessagePacket();
			packetToServer.client_name = getName();

			if(!tryforward()) {
				// If we fail...
				if(randomGen.nextInt(3) == 1) {
					// turn left!
					packetToServer.type = MessagePacket.CLIENT_LEFT;
				} else {
					// or perhaps turn right!
					packetToServer.type = MessagePacket.CLIENT_RIGHT;
				}
			} else packetToServer.type = MessagePacket.CLIENT_UP;

			if(active && !permanent_deactivation)
			{
				maze.multicastEvent(packetToServer, true);
			}

			// Shoot at things once and a while.
			if(true) {
				MessagePacket anotherPacket = new MessagePacket();
				anotherPacket.client_name = getName();
				anotherPacket.type = MessagePacket.CLIENT_SPACE;

				if(active && !permanent_deactivation)
				{
					maze.multicastEvent(anotherPacket, true);
				}
			}


			// Sleep so the humans can possibly compete.
			try {
				Thread.sleep(100);
			} catch(Exception e) {
				// Shouldn't happen.
			}
		}
	}
}
