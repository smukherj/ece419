import java.io.*;
import java.lang.Thread;
import java.util.concurrent.locks.ReentrantLock;

public class BulletTickerThread extends Thread {

	private MazeImpl maze = null;
	private ReentrantLock m_lock = null;

	public BulletTickerThread(MazeImpl maze, ReentrantLock lock){
		super("BulletTickerThread");
		assert(maze != null);
		this.maze = maze;
		m_lock = lock;
	}

	public void run(){
		System.out.println("Info: Bullet ticker thread is running.");
		m_lock.lock();
		try
		{
			while(!isInterrupted()){
				MessagePacket tickerPacket = new MessagePacket();
				tickerPacket.type = MessagePacket.CLIENT_TICK;

				maze.multicastEvent(tickerPacket, true);

				try{
					Thread.sleep(200);
				}
				catch(InterruptedException e){
					break;
				}
			}
		}
		finally
		{
			if(m_lock.isHeldByCurrentThread())
			{
				m_lock.unlock();
			}
		}

	}
}
