
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class CoordinationServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ReentrantLock lock = new ReentrantLock();
		int port = 7000;
		
		try
		{
			port = Integer.parseInt(args[0]);
		}
		catch(NumberFormatException e)
		{
			
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			
		}
		System.out.println("Info: Attempting to launch Coordination Server on port: " + port);
		
		ServerSocket socket = null;
		try
		{
			socket = new ServerSocket(port);
		}
		catch(IOException e)
		{
			System.err.println("Error: Failed to listen at specified port. Got IO exception");
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
		catch(IllegalArgumentException e)
		{
			System.err.println("Error: Failed to listen at specified port. Got IllegalArgument exception");
			System.err.println("Error: " + e.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Info: Listening for incoming connections on " + socket.getInetAddress().getHostAddress() +
				":" + socket.getLocalPort());
		Socket clientsock;
		while(true)
		{
			try
			{
				while((clientsock = socket.accept()) != null)
				{
					new CoordinationClientHandler(clientsock, lock).start();
				}
			}
			catch(IOException e)
			{
				// ignore failed connection attempts
			}
		}
	}
}
