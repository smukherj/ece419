import java.net.*;
import java.io.*;
import java.util.*;

public class OnlineBrokerHandlerThread extends Thread {
	private Socket socket = null;

	public OnlineBrokerHandlerThread(Socket socket) {
		super("OnlineBrokerHandlerThread");
		this.socket = socket;
		System.out.println("Created new Thread to handle client");
	}
	
	public void handle_client_request(BrokerPacket packetFromClient,
									BrokerPacket packetToClient)
	{
		String formatted_symbol = packetFromClient.symbol;
		packetToClient.type = BrokerPacket.BROKER_QUOTE;
		Long quote = m_quotes.get(formatted_symbol.toLowerCase().trim());
		if(quote == null)
		{
			quote = new Long(0);
		}
		packetToClient.quote = quote;
	}
	
	public boolean handle_client_packet(BrokerPacket packetFromClient,
										ObjectOutputStream toClient)
										throws IOException
	{
		BrokerPacket packetToClient = new BrokerPacket();
		switch(packetFromClient.type)
		{
		case BrokerPacket.BROKER_NULL: return true;
		case BrokerPacket.BROKER_REQUEST: handle_client_request(packetFromClient, packetToClient);
		break;
		case BrokerPacket.BROKER_BYE: return false;
		default: return false;
		}
		toClient.writeObject(packetToClient);
		return true;
	}
	
	public synchronized void init_table() throws FileNotFoundException
	{
		if(m_quotes != null)
		{
			return;
		}
		
		m_quotes = new HashMap<String, Long>();
		
		FileReader fr = new FileReader("nasdaq");
		BufferedReader br = new BufferedReader(fr);
		
		String line = null;
		try{
			while((line = br.readLine()) != null)
			{
				String[] split_line = line.split(" ");
				String symbol = split_line[0].toLowerCase().trim();
				long quote = Long.parseLong(split_line[1]);
				m_quotes.put(symbol, quote);
			}
		}
		catch (IOException e)
		{
			
		}
	}

	public void run() {

		boolean gotByePacket = false;
		if(m_quotes == null)
		{
			try {
				init_table();
			}
			catch(FileNotFoundException e)
			{
				System.err.println("Error: Could not open the nasdaq quotes file");
				return;
			}
		}
		
		try {
			/* stream to read from client */
			ObjectInputStream fromClient = new ObjectInputStream(socket.getInputStream());
			BrokerPacket packetFromClient;
			
			/* stream to write back to client */
			ObjectOutputStream toClient = new ObjectOutputStream(socket.getOutputStream());
			

			while (( packetFromClient = (BrokerPacket) fromClient.readObject()) != null) {
				if(!handle_client_packet(packetFromClient, toClient))
					break;
			}
			
			/* cleanup when client exits */
			fromClient.close();
			toClient.close();
			socket.close();

		} catch (IOException e) {
			if(!gotByePacket)
				e.printStackTrace();
		} catch (ClassNotFoundException e) {
			if(!gotByePacket)
				e.printStackTrace();
		}
	}
	
	public static HashMap<String, Long> m_quotes = null;
}
